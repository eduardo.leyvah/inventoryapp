import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter/services.dart';

import 'package:inventario_app/src/routes/routes.dart';
 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    SystemChrome.setPreferredOrientations([
        DeviceOrientation.portraitUp,
        DeviceOrientation.portraitDown,
    ]);
    
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate
      ],
      supportedLocales: [
          const Locale('en', 'US'), // Ingles
          const Locale('es', 'ES') // Español 
      ],
      title: 'Material App',
      initialRoute: 'login',
      routes: getApplicationRoutes(),
      theme: ThemeData(
        primaryColor: Color.fromRGBO(70, 52, 167, 1.0)
      ),
    );
  }
}