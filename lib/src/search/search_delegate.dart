import 'package:flutter/material.dart';

import 'package:inventario_app/src/providers/db_provider.dart';

class DataSearch extends SearchDelegate{

  final db = DBProvider.db;
  
  @override
  List<Widget> buildActions(BuildContext context) {
      // Las acciones de nuestro AppBar
      return [
        IconButton(
          icon: Icon(Icons.clear), 
          onPressed: () {
            query = '';
          },
        )
      ];
  }
  
    @override
    Widget buildLeading(BuildContext context) {
       // Icono a la izquierda del AppBar
      return IconButton(
        icon: AnimatedIcon(
          icon: AnimatedIcons.menu_arrow, 
          progress: transitionAnimation
        ), 
        onPressed: () {
          close(context, null);
        }
      );
    }
  
    @override
    Widget buildResults(BuildContext context) {
      // Crea los resultados que vamos a mostrar
      return Container();
    }
  
    @override
    Widget buildSuggestions(BuildContext context) {
      // Son las sugerencias que aparecen cuando la persona escribe
      if(query.isEmpty){
        return Container();
      }

      return FutureBuilder(
        future: db.getProductosNombre(query),
        builder: (BuildContext context, AsyncSnapshot<List<ProductoModel>> snapshot) {

          if( snapshot.hasData ){

            final productos = snapshot.data;

            return ListView(
              children: productos.map( ( producto ) {
                return ListTile(
                  leading: Icon(
                    Icons.archive,
                    size: 20,
                  ),
                  title: Text(producto.nombre),
                  onTap: () {
                    close(context, null);
                    Navigator.pushNamed(context, 'detalle', arguments: producto);
                  },
                );
              }).toList()
            );

          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }

        }
      );

    }



}