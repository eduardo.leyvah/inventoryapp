import 'package:flutter/material.dart';
import 'package:inventario_app/src/bloc/gasto_producto_bloc.dart';
import 'package:inventario_app/src/bloc/producto_bloc.dart';
import 'package:inventario_app/src/models/gasto_model.dart';
// import 'package:inventario_app/src/bloc/venta_bloc.dart';
import 'package:inventario_app/src/models/producto_model.dart';
// import 'package:inventario_app/src/models/venta_model.dart';
import 'package:inventario_app/src/providers/db_provider.dart';

class CardGastoProductoPage extends StatefulWidget{
  
  
  @override
  _CardGastoProductoPageState createState() => _CardGastoProductoPageState();
}

class _CardGastoProductoPageState extends State<CardGastoProductoPage> {

  final productoBloc = new ProductoBloc();
  final gastosBloc = new GastoProductoBloc();
  List<ProductoModel> _res;
  String _nombreProducto = '';

  @override
  void initState() {
    super.initState();
    getProductos();
    gastosBloc.obtenerGastoProducto();
  }

  @override
  Widget build(BuildContext context) {
    return _cardVenta();
  }

  Widget _simplePopup(BuildContext context, GastoModel gasto, int i) => PopupMenuButton<int>(
    itemBuilder: (context) => [
          PopupMenuItem(
            value: 1,
            child: Text("Editar"),
          ),
          PopupMenuItem(
            value: 2,
            child: Text("Cancelar Gasto"),
          ),
    ],
    child: Icon(Icons.more_vert),
    onSelected: (value) {
      if(value == 1){
        // Navigator.pushNamed(context, 'editar_venta', arguments: {'fecha': venta.fecha, 'id_producto': venta.idProducto, 'iterable': i, 'cantidad':venta.cantidad, 'total':venta.totalVenta });
        Navigator.pushNamed(context, 'editar_gastop', arguments: {'id_gasto': gasto.idGasto, 'fecha': gasto.fecha, 'id_producto': gasto.idProducto, 'iterable': i, 'cantidad':gasto.cantidad, 'total':gasto.totalCompra });
      }else{
        gastosBloc.updateCancelarGasto(gasto.cantidad, gasto.idProducto);
        gastosBloc.borrarGastoP(gasto.idGasto);
      }
    },
  );

  Widget _cardVenta() {

    gastosBloc.obtenerGastoProducto();

    return StreamBuilder<List<GastoModel>>(
      stream: gastosBloc.gastoProdcutoStream ,
      builder: (BuildContext context, AsyncSnapshot<List<GastoModel>> snapshot){
        
        if ( !snapshot.hasData ){
          return Center(child: CircularProgressIndicator());
        }
        final gasto = snapshot.data;
        
        if ( gasto.length == 0 ){
          return Center(child: Text('No hay información'));
        }

        return ListView.builder(
          shrinkWrap: true,
          physics: ScrollPhysics(),
          itemCount: gasto.length,
          itemBuilder: (BuildContext context, int i) => Card(
            elevation: 5,
            child: ClipPath(
              child: Container(
                decoration: BoxDecoration(
                  border: Border(left: BorderSide(color: Colors.red, width: 7.0))
                ),
                child: ListTile(
                  title: Text("Venta de "+ getNombreProducto(gasto[i].idProducto)),
                  subtitle: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(height: 5),
                      Text('Fecha: ${gasto[i].fecha}'),
                      SizedBox(height: 5),
                      Text('Cantidad: ${gasto[i].cantidad.toString()}'),
                      SizedBox(height: 5),
                      Text(r'Total: $'+'${gasto[i].totalCompra.toString()}')
                    ],
                  ),
                  trailing: _simplePopup(context, gasto[i], i),
                ),
              ),
              clipper: ShapeBorderClipper(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(3)
                )
              ),
            ),
          )
        );
        
      },
    );
  }

  Future<List<ProductoModel>> getProductos() async {
    final db = DBProvider.db;
    _res = await db.getTodosProductos(); 
    return _res;
  }

  String getNombreProducto(int idProducto){
    for (var i = 0; i < _res.length; i++) {
      if(idProducto == _res[i].idProducto){
        _nombreProducto = _res[i].nombre;
      }
    }
    return _nombreProducto;
  }


}