import 'package:flutter/material.dart';
import 'package:inventario_app/src/bloc/categoria_bloc.dart';
import 'package:inventario_app/src/models/categoria_model.dart';

class CardCategoriaWidget extends StatefulWidget{
  
  @override
  _CardCategoriaWidgetState createState() => _CardCategoriaWidgetState();
}

class _CardCategoriaWidgetState extends State<CardCategoriaWidget> {
  
  final categoriaBloc = new CategoriaBloc();

  String _nombreCategoria = '';

  TextEditingController _controllerInput = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return _cardCategoria();
  }

  Widget _cardCategoria(){
    categoriaBloc.obtenerCategorias();

    return StreamBuilder<List<CategoriaModel>>(
      stream: categoriaBloc.categoriasStream ,
      builder: (BuildContext context, AsyncSnapshot<List<CategoriaModel>> snapshot){
        
        if ( !snapshot.hasData ){
          return Center(child: CircularProgressIndicator());
        }
        final categorias = snapshot.data;
        
        if ( categorias.length == 0 ){
          return Center(child: Text('No hay información'));
        }

        return ListView.builder(
          itemCount: categorias.length,
          itemBuilder: (BuildContext context, int i) => Card(
            elevation: 5,
            child: ClipPath(
              child: Container(
                decoration: BoxDecoration(
                  border: Border(left: BorderSide(color: Color.fromRGBO(61, 59, 187, 1.0), width: 7.0))
                ),
                child: ListTile(
                  title: Text(categorias[i].nombre),
                  trailing: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      _crearBotonesTrailing(context, categorias[i].idCategoria, categorias[i].nombre, Colors.amber, Colors.amber[800], Icon(Icons.mode_edit, color: Colors.white,), 1),
                      SizedBox(width: 15.0),
                      _crearBotonesTrailing(context, categorias[i].idCategoria, categorias[i].nombre, Colors.red, Colors.red[900], Icon(Icons.delete, color: Colors.white,), 0)
                    ],
                  ),
                ),
              ),
              clipper: ShapeBorderClipper(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(3)
                )
              ),
            ),
          )
        );
        
      },
    );
  }

  Widget _crearBotonesTrailing( BuildContext context, int id ,String nombre, Color backColor, Color splashColor, Icon icon, int flag){

    return ClipOval(
      child: Material(
        color: backColor, // button color
        child: InkWell(
          splashColor: splashColor, // inkwell color
          child: SizedBox(width: 40, height: 40, child: icon),
          onTap: () {
            if ( flag == 1 ){
              _mostrarAlert(context, id, nombre);
            }else if (flag == 0){
              categoriaBloc.borrarCategoria(id);
            }
          },
        ),
      ),
    );

  }

  void _mostrarAlert(BuildContext context, int id, String nombre){

      showDialog(
        
        context: context,
        barrierDismissible: false,
        builder: (context){

          return AlertDialog(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(17.0)),
            title: Text('Editar Categoría'),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                _crearInputEdit(nombre)
              ],
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('Cancelar', style: TextStyle(color: Colors.red),),
                onPressed: () => Navigator.of(context).pop(),
              ),
              FlatButton(
                child: Text('Aceptar'),
                onPressed: () {
                  final categoria = CategoriaModel(idCategoria: id, nombre:_nombreCategoria);
                  categoriaBloc.updateCategoria(categoria);
                  Navigator.of(context).pop();
                },
              ),
            ],
          );

        }

      );

  }

  Widget _crearInputEdit(String nombre) {
    _controllerInput = new TextEditingController(text: nombre);
    return TextField(
      //autofocus: true,
      controller: _controllerInput,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15.0)
        ),
        hintText: nombre,
        labelText: 'Editar categoría',
        icon: Icon(Icons.category)
      ),
      onChanged: (valor) => setState(() {
          _nombreCategoria = valor;
      })
    );
  }
}