import 'package:flutter/material.dart';
import 'package:inventario_app/src/bloc/producto_bloc.dart';
import 'package:inventario_app/src/models/producto_model.dart';

class CardProductoWidget extends StatefulWidget{
  
  
  @override
  _CardProductoWidgetState createState() => _CardProductoWidgetState();
}

class _CardProductoWidgetState extends State<CardProductoWidget> {

  final productoBloc = new ProductoBloc();

  @override
  Widget build(BuildContext context) {
    return _cardProducto();
  }

  Widget _cardProducto() {

    productoBloc.obtenerProductos();

    return StreamBuilder<List<ProductoModel>>(
      stream: productoBloc.productosStream ,
      builder: (BuildContext context, AsyncSnapshot<List<ProductoModel>> snapshot){
        
        if ( !snapshot.hasData ){
          return Center(child: CircularProgressIndicator());
        }
        final productos = snapshot.data;
        
        if ( productos.length == 0 ){
          return Center(child: Text('No hay información'));
        }

        return ListView.builder(
          shrinkWrap: true,
          physics: ScrollPhysics(),
          itemCount: productos.length,
          itemBuilder: (BuildContext context, int i) => Card(
            elevation: 5,
            child: ClipPath(
              child: Container(
                decoration: BoxDecoration(
                  border: Border(left: BorderSide(color: Color.fromRGBO(61, 59, 187, 1.0), width: 7.0))
                ),
                child: ListTile(
                  title: Text(productos[i].nombre),
                  subtitle: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(height: 5),
                      Text('Cantidad: ${productos[i].cantidad.toString()}'),
                      SizedBox(height: 5),
                      Text(r'Precio: $'+'${productos[i].precioVenta.toString()}')
                    ],
                  ),
                  trailing: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      _crearBotonesTrailing(context, productos[i] ,Colors.amber, Colors.amber[800], Icon(Icons.mode_edit, color: Colors.white,), 1),
                      SizedBox(width: 15.0),
                      _crearBotonesTrailing(context, productos[i] ,Colors.red, Colors.red[900], Icon(Icons.delete, color: Colors.white,), 0)
                    ],
                  ),
                ),
              ),
              clipper: ShapeBorderClipper(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(3)
                )
              ),
            ),
          )
        );
        
      },
    );
  }

  Widget _crearBotonesTrailing( BuildContext context, ProductoModel producto, Color backColor, Color splashColor, Icon icon, int flag){

    return ClipOval(
      child: Material(
        color: backColor, // button color
        child: InkWell(
          splashColor: splashColor, // inkwell color
          child: SizedBox(width: 40, height: 40, child: icon),
          onTap: () {
            if ( flag == 1 ){
              Navigator.pushNamed(context, 'editar_producto', arguments: {'producto':producto, 'id_categoria':producto.catId});
            }else if (flag == 0){
              productoBloc.borrarProducto(producto.idProducto);
            }
          },
        ),
      ),
    );

  }
}