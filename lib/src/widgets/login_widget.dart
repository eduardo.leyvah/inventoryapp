import 'package:flutter/material.dart';

import 'package:barcode_scan/barcode_scan.dart';

class LoginForm extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return Container(
      child: _crearLogin(context),
    );
  }

  Widget _crearLogin( BuildContext context ){

    final size = MediaQuery.of(context).size;

    final login = Container(
      height: size.width > 320 ? size.height * 0.58 : size.height * 0.55,
      width: double.infinity,
      decoration: BoxDecoration(
        borderRadius: size.width > 320 ? BorderRadius.only(topLeft: Radius.circular(40.0), topRight: Radius.circular(40.0))
                                       : BorderRadius.only(topLeft: Radius.circular(45.0), topRight: Radius.circular(45.0)),
        //borderRadius: BorderRadius.only(topLeft: Radius.circular(40.0), topRight: Radius.circular(40.0)),
        color: Colors.white,
        boxShadow: <BoxShadow>[
                BoxShadow(
                  color: Colors.black38,
                  blurRadius: 5.0,
                  offset: Offset(0.0, - 5.0),
                  spreadRadius: 4.0
                )
        ]
      ),
      
    );

    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        Stack(
          children: <Widget>[
            login,
            Container(
              padding: size.width > 320 ? EdgeInsets.all(80.0) : EdgeInsets.all(60.0),
              child: Column(
                children: <Widget>[
                  size.width > 320 ? SizedBox(height: 25.0) : SizedBox(height: 60.0)  ,
                  //  _crearInputUser(),
                  // size.width < 412 ? SizedBox(height: 15.0) : SizedBox(height: 25.0),
                  // _crearInputPassword(),
                  // SizedBox(height: 25.0),
                  _crearBoton(context)
                ],
              ),
            )
          ],
        ),
      ],
    );

  }


  Widget _crearBoton(BuildContext context){

    return RaisedButton.icon(
      padding: EdgeInsets.symmetric(horizontal: 55, vertical: 25),
      icon: Icon(Icons.camera, size: 25,),
      label: Text('Ingresar'),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      color: Color.fromRGBO(70, 52, 167, 1.0),
      textColor: Colors.white,
      elevation: 0.0,
      onPressed: () {
        _scanQR(context);
      }
    );

  }

  _scanQR(BuildContext context) async{

    var scan;
    String futureString;
    String key = '63815D7BE0EDD3C8D6A33F5DB19F86EE9CFC9A1E5A458420AE84F4AB1D00ED94';
    final snack = SnackBar(
      backgroundColor: Colors.red,
      content: Text('Login Fallido')
    );
    //String futureString = 'https://fernando-herrera.com';

    try {
      scan = await BarcodeScanner.scan();
      futureString = scan.rawContent;
      print(futureString);
    } catch (e) {
      futureString = e.toString();
    }

    if ( futureString == key ){
      Navigator.pushNamed(context, 'home');
    } else{
      Scaffold.of(context).showSnackBar(snack);
    }


  }


  

}