import 'package:flutter/material.dart';
import 'package:inventario_app/src/bloc/producto_bloc.dart';
import 'package:inventario_app/src/bloc/venta_bloc.dart';
import 'package:inventario_app/src/models/producto_model.dart';
import 'package:inventario_app/src/models/venta_model.dart';
import 'package:inventario_app/src/providers/db_provider.dart';

class CardVentaWidget extends StatefulWidget{
  
  
  @override
  _CardVentaWidgetState createState() => _CardVentaWidgetState();
}

class _CardVentaWidgetState extends State<CardVentaWidget> {

  final productoBloc = new ProductoBloc();
  final ventasBloc = new VentasBloc();
  List<ProductoModel> _res;
  String _nombreProducto = '';

  @override
  void initState() {
    super.initState();
    getProductos();
    ventasBloc.obtenerVenta();
  }

  @override
  Widget build(BuildContext context) {
    return _cardVenta();
  }

  Widget _simplePopup(BuildContext context, VentaModel venta, int i) => PopupMenuButton<int>(
    itemBuilder: (context) => [
          PopupMenuItem(
            value: 1,
            child: Text("Editar"),
          ),
          PopupMenuItem(
            value: 2,
            child: Text("Cancelar Venta"),
          ),
    ],
    child: Icon(Icons.more_vert),
    onSelected: (value) {
      if(value == 1){
        Navigator.pushNamed(context, 'editar_venta', arguments: {'id_venta':venta.idVenta,'fecha': venta.fecha, 'id_producto': venta.idProducto, 'iterable': i, 'cantidad':venta.cantidad, 'total':venta.totalVenta });
      }else{
        ventasBloc.updateCancelarVenta(venta.cantidad, venta.idProducto);
        ventasBloc.borrarVenta(venta.idVenta);
      }
    },
  );

  Widget _cardVenta() {

    ventasBloc.obtenerVenta();

    return StreamBuilder<List<VentaModel>>(
      stream: ventasBloc.ventaStream ,
      builder: (BuildContext context, AsyncSnapshot<List<VentaModel>> snapshot){
        
        if ( !snapshot.hasData ){
          return Center(child: CircularProgressIndicator());
        }
        final ventas = snapshot.data;
        
        if ( ventas.length == 0 ){
          return Center(child: Text('No hay información'));
        }

        return ListView.builder(
          itemCount: ventas.length,
          itemBuilder: (BuildContext context, int i) => Card(
            elevation: 5,
            child: ClipPath(
              child: Container(
                decoration: BoxDecoration(
                  border: Border(left: BorderSide(color: Colors.green, width: 7.0))
                ),
                child: ListTile(
                  title: Text("Venta de "+ getNombreProducto(ventas[i].idProducto)),
                  subtitle: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(height: 5),
                      Text('Fecha: ${ventas[i].fecha}'),
                      SizedBox(height: 5),
                      Text('Cantidad: ${ventas[i].cantidad.toString()}'),
                      SizedBox(height: 5),
                      Text(r'Total: $'+'${ventas[i].totalVenta.toString()}')
                    ],
                  ),
                  trailing: _simplePopup(context, ventas[i], i),
                ),
              ),
              clipper: ShapeBorderClipper(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(3)
                )
              ),
            ),
          )
        );
        
      },
    );
  }

  // Widget _crearBotonesTrailing( BuildContext context, ProductoModel producto, Color backColor, Color splashColor, Icon icon, int flag){

  //   return ClipOval(
  //     child: Material(
  //       color: backColor, // button color
  //       child: InkWell(
  //         splashColor: splashColor, // inkwell color
  //         child: SizedBox(width: 40, height: 40, child: icon),
  //         onTap: () {
  //           if ( flag == 1 ){
  //             Navigator.pushNamed(context, 'editar_producto', arguments: producto);
  //           }else if (flag == 0){
  //             productoBloc.borrarProducto(producto.idProducto);
  //           }
  //         },
  //       ),
  //     ),
  //   );

  // }

  Future<List<ProductoModel>> getProductos() async {
    final db = DBProvider.db;
    _res = await db.getTodosProductos(); 
    return _res;
  }

  String getNombreProducto(int idProducto){
    
    if(_res == null){
      getProductos().then((val) => setState(() {
        _res = val;
      }));
    } else {
      for (var i = 0; i < _res.length; i++) {
        if(idProducto == _res[i].idProducto){
          _nombreProducto = _res[i].nombre;
        }
      }      
    }
    return _nombreProducto;
  }


}