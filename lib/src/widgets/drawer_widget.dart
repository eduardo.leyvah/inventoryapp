import 'package:flutter/material.dart';

class DrawerWidget extends StatelessWidget{
  
  @override
  Widget build(BuildContext context) {
    return _crearDrawer(context);
  }

   Widget _crearDrawer(BuildContext context){

    final size = MediaQuery.of(context).size;

    return Container(
      width: size.width < 412 ? 210.0 : 315.0,
      child: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Image(
                image: AssetImage('assets/inventory-logo.png')
              ),
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: <Color> [
                    Color.fromRGBO(70, 52, 167, 1.0),
                    Color.fromRGBO(81, 40, 136, 1.0),
                  ]
                ),
              ),
            ),
            ListTile(
              leading: Icon(Icons.store, color: Color.fromRGBO(81, 40, 136, 1.0)),
              title: Text('Home'),
              onTap: () => Navigator.pushNamed(context, 'home'),
            ),
            ListTile(
              leading: Icon(Icons.dashboard, color: Color.fromRGBO(81, 40, 136, 1.0)),
              title: Text('Dashboard'),
              onTap: () => Navigator.pushNamed(context, 'dashboard'),
            ),
            ListTile(
              leading: Icon(Icons.insert_chart, color: Color.fromRGBO(81, 40, 136, 1.0)),
              title: Text('Ventas'),
              onTap: () => Navigator.pushNamed(context, 'ventas')
            ),
            ListTile(
              leading: Icon(Icons.monetization_on, color: Color.fromRGBO(81, 40, 136, 1.0)),
              title: Text('Gastos'),
              onTap: () => Navigator.pushNamed(context, 'gastos')
            ),
            ListTile(
              leading: Icon(Icons.exit_to_app, color: Color.fromRGBO(81, 40, 136, 1.0)),
              title: Text('Cerrar Sesión'),
              onTap: () => Navigator.pushNamedAndRemoveUntil(context,'login', (Route<dynamic> route) => false)
            ),
          ],
        ),
      ),
    );

  }

}