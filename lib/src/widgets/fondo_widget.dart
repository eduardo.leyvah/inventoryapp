import 'package:flutter/material.dart';

class FondoWidget extends StatelessWidget{
  
  @override
  Widget build(BuildContext context) {
    return Container(
      child: _crearFondo(context)
    );
  }

  Widget _crearFondo(BuildContext context) {

    final size = MediaQuery.of(context).size;

    final fondoModaro = Container(
      height: double.infinity,
      width: double.infinity,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: <Color> [
            Color.fromRGBO(70, 52, 167, 1.0),
            Color.fromRGBO(81, 40, 136, 1.0),
          ]
        )
      ),
    );

    final circulo = Container(
      width: 100.0,
      height: 100.0,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(100.0),
        color: Color.fromRGBO(255, 255, 255, 0.05)
      ),
    );


    return Stack(
      children: <Widget>[
        fondoModaro,
        Positioned( top: 90.0, left: 30.0, child: circulo ),
        Positioned( top: -40.0, right: -30.0, child: circulo ),
        Positioned( bottom: -50.0, right: -10.0, child: circulo ),
        Positioned( bottom: 120.0, right: 20.0, child: circulo ),
        Positioned( bottom: -50.0, left: -20.0, child: circulo ),
        
        Container(
          padding: size.width < 412 ?  EdgeInsets.only(top: 40.0) : EdgeInsets.only(top: 85.0),
          //padding: EdgeInsets.only(top: 50.0),
          child: Column(
            children: <Widget>[
              //Icon( Icons.person_pin_circle, color: Colors.white, size: 100.0 ),
              Image(
                image: AssetImage('assets/inventory-logo.png'),
                width: size.width < 412 ? 190 : 250.0,
              ),
              SizedBox( height: 10.0, width: double.infinity ),
              //Text('Fernando Herrera', style: TextStyle( color: Colors.white, fontSize: 25.0 ))
            ],
          ),
        )

      ],
    );

  }


}