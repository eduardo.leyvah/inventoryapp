import 'package:flutter/material.dart';
import 'package:inventario_app/src/bloc/otro_gasto_bloc.dart';
import 'package:inventario_app/src/providers/db_provider.dart';

class CardOtroGastoPage extends StatefulWidget{
  
  
  @override
  _CardOtroGastoPageState createState() => _CardOtroGastoPageState();
}

class _CardOtroGastoPageState extends State<CardOtroGastoPage> {

  final otroGastoBloc = new OtrosGastosBloc();

  @override
  void initState() {
    super.initState();
    otroGastoBloc.obtenerOtroGasto();
  }

  @override
  Widget build(BuildContext context) {
    return _cardGasto();
  }

  Widget _simplePopup(BuildContext context, OtroGastoModel gasto) => PopupMenuButton<int>(
    itemBuilder: (context) => [
          PopupMenuItem(
            value: 1,
            child: Text("Editar"),
          ),
          PopupMenuItem(
            value: 2,
            child: Text("Cancelar Gasto"),
          ),
    ],
    child: Icon(Icons.more_vert),
    onSelected: (value) {
      if(value == 1){
        Navigator.pushNamed(context, 'editar_ogasto', arguments: {'id_gasto': gasto.idGastoOtro, 'concepto':gasto.concepto, 'fecha':gasto.fecha, 'total':gasto.totalGasto});
      }else{
        otroGastoBloc.borrarOtroGasto(gasto.idGastoOtro);
      }
    },
  );

  Widget _cardGasto() {

    otroGastoBloc.obtenerOtroGasto();

    return StreamBuilder<List<OtroGastoModel>>(
      stream: otroGastoBloc.otroGastoStream ,
      builder: (BuildContext context, AsyncSnapshot<List<OtroGastoModel>> snapshot){
        
        if ( !snapshot.hasData ){
          return Center(child: CircularProgressIndicator());
        }
        final gastos = snapshot.data;
        
        if ( gastos.length == 0 ){
          return Center(child: Text('No hay información'));
        }

        return ListView.builder(
          shrinkWrap: true,
          physics: ScrollPhysics(),
          itemCount: gastos.length,
          itemBuilder: (BuildContext context, int i) => Card(
            elevation: 5,
            child: ClipPath(
              child: Container(
                decoration: BoxDecoration(
                  border: Border(left: BorderSide(color: Colors.orange, width: 7.0))
                ),
                child: ListTile(
                  title: Text("Gasto de "+ gastos[i].concepto),
                  subtitle: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(height: 5),
                      Text('Fecha: ${gastos[i].fecha}'),
                      SizedBox(height: 5),
                      Text(r'Total: $'+'${gastos[i].totalGasto.toString()}')
                    ],
                  ),
                  trailing: _simplePopup(context, gastos[i]),
                ),
              ),
              clipper: ShapeBorderClipper(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(3)
                )
              ),
            ),
          )
        );
        
      },
    );
  }


}