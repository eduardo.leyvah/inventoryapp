class OtroGastoModel {
    OtroGastoModel({
        this.idGastoOtro,
        this.concepto,
        this.totalGasto,
        this.fecha,
    });

    int idGastoOtro;
    String concepto;
    double totalGasto;
    String fecha;

    factory OtroGastoModel.fromJson(Map<String, dynamic> json) => OtroGastoModel(
        idGastoOtro : json["id_gasto_otro"],
        concepto    : json["concepto"],
        totalGasto  : json["total_gasto"].toDouble(),
        fecha       : json["fecha"],
    );

    Map<String, dynamic> toJson() => {
        "id_gasto_otro" : idGastoOtro,
        "concepto"      : concepto,
        "total_gasto"   : totalGasto,
        "fecha"         : fecha,
    };
}
