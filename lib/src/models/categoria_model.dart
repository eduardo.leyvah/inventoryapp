class CategoriaModel {
    
    int idCategoria;
    String nombre;
    
    CategoriaModel({
        this.idCategoria,
        this.nombre,
    });

    factory CategoriaModel.fromJson(Map<String, dynamic> json) => CategoriaModel(
        idCategoria : json["id_categoria"],
        nombre      : json["nombre_categoria"],
    );

    Map<String, dynamic> toJson() => {
        "id_categoria"     : idCategoria,
        "nombre_categoria" : nombre,
    };
}