class ProductoModel {
    ProductoModel({
        this.idProducto,
        this.nombre,
        this.catId,
        this.precioVenta,
        this.precioCompra,
        this.cantidad,
    });

    int idProducto;
    String nombre;
    int catId;
    double precioVenta;
    double precioCompra;
    int cantidad;

    factory ProductoModel.fromJson(Map<String, dynamic> json) => ProductoModel(
        idProducto   : json["id_producto"],
        nombre       : json["nombre_producto"],
        catId        : json["cat_id"],
        precioVenta  : json["precio_venta"].toDouble(),
        precioCompra : json["precio_compra"].toDouble(),
        cantidad     : json["cantidad"],
    );

    Map<String, dynamic> toJson() => {
        "id_producto"     : idProducto,
        "nombre_producto" : nombre,
        "cat_id"          : catId,
        "precio_venta"    : precioVenta,
        "precio_compra"   : precioCompra,
        "cantidad"        : cantidad,
    };
}