class VentaModel {
    VentaModel({
        this.idVenta,
        this.idProducto,
        this.cantidad,
        this.totalVenta,
        this.fecha,
    });

    int idVenta;
    int idProducto;
    int cantidad;
    double totalVenta;
    String fecha;

    factory VentaModel.fromJson(Map<String, dynamic> json) => VentaModel(
        idVenta     : json["id_venta"],
        idProducto  : json["id_producto"],
        cantidad    : json["cantidad"],
        totalVenta  : json["total_venta"].toDouble(),
        fecha       : json["fecha"],
    );

    Map<String, dynamic> toJson() => {
        "id_venta"    : idVenta,
        "id_producto" : idProducto,  
        "cantidad"    : cantidad  ,
        "total_venta" : totalVenta,
        "fecha"       : fecha
    };
}