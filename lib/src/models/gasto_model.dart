class GastoModel {
    GastoModel({
        this.idGasto,
        this.idProducto,
        this.cantidad,
        this.totalCompra,
        this.fecha
    });

    int idGasto;
    int idProducto;
    int cantidad;
    double totalCompra;
    String fecha;

    factory GastoModel.fromJson(Map<String, dynamic> json) => GastoModel(
        idGasto     : json["id_gasto"],
        idProducto  : json["id_producto"],
        cantidad    : json["cantidad"],
        totalCompra  : json["total_gasto"].toDouble(),
        fecha       : json["fecha"],
    );

    Map<String, dynamic> toJson() => {
        "id_gasto"    : idGasto,
        "id_producto" : idProducto ,
        "cantidad"    : cantidad,   
        "total_gasto" : totalCompra,
        "fecha"       : fecha
    };
}