import 'package:flutter/material.dart';
import 'package:inventario_app/src/widgets/card_venta_widget.dart';


class VentasPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Ventas'),
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: <Color> [
                Color.fromRGBO(70, 52, 167, 1.0),
                Color.fromRGBO(81, 40, 136, 1.0),
              ]
            )
          ),
        ),
        actions: <Widget>[
          IconButton(
             icon: Icon(Icons.add ), 
             onPressed: () {
               Navigator.pushNamed(context, 'nueva_venta');
             }
          ),
        ],
      ),
      body: _crearCard(),
    );
  }

  Widget _crearCard(){
    return CardVentaWidget();
  }

}