import 'package:flutter/material.dart';

import 'package:inventario_app/src/bloc/categoria_bloc.dart';
import 'package:inventario_app/src/models/categoria_model.dart';

import 'package:inventario_app/src/widgets/card_categoria_widget.dart';
import 'package:inventario_app/src/widgets/drawer_widget.dart';


class CategoriasPage extends StatefulWidget {

  @override
  _CategoriasPageState createState() => _CategoriasPageState();
}

class _CategoriasPageState extends State<CategoriasPage> {
  final categoriaBloc = new CategoriaBloc();

  String _nombreCategoria = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        title: Text('Categorías'),
        actions: <Widget>[
           IconButton(
             icon: Icon(Icons.add ), 
             onPressed: () {
               _mostrarAlert(context);
             }
          ),
        ],
      ),
      body: _crearCard(),
      drawer: _crearDrawer()
    );
  }

  Widget _crearDrawer() {
    return DrawerWidget();
  }

  Widget _crearCard(){
    return CardCategoriaWidget();
  }

  void _mostrarAlert(BuildContext context){

      showDialog(
        
        context: context,
        barrierDismissible: false,
        builder: (context){

          return AlertDialog(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(17.0)),
            title: Text('Nueva Categoría'),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                _crearInputCategoria()
              ],
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('Cancelar', style: TextStyle(color: Colors.red),),
                onPressed: () => Navigator.of(context).pop(),
              ),
              FlatButton(
                child: Text('Agregar'),
                onPressed: () {
                  final categoria = CategoriaModel(nombre:_nombreCategoria);
                  categoriaBloc.agregarCategoria(categoria);
                  Navigator.of(context).pop();
                },
              ),
            ],
          );

        }

      );

  }

  Widget _crearInputCategoria() {
  
    return TextField(
      //autofocus: true,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15.0)
        ),
        hintText: 'Nombre de categoría',
        labelText: 'Categoría',
        icon: Icon(Icons.category)
      ),
      onChanged: (valor) => setState(() {
          _nombreCategoria = valor;
      })
    );
  }
}