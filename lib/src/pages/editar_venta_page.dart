import 'package:flutter/material.dart';

import 'package:intl/intl.dart';
import 'package:inventario_app/src/bloc/venta_bloc.dart';

import 'package:inventario_app/src/bloc/producto_bloc.dart';
import 'package:inventario_app/src/models/producto_model.dart';
import 'package:inventario_app/src/models/venta_model.dart';
import 'package:inventario_app/src/providers/db_provider.dart';


class EditarVentaPage extends StatefulWidget {
  @override
   _EditarVentaPageState createState() => _EditarVentaPageState();
}
class _EditarVentaPageState extends State<EditarVentaPage> {

  final productoBloc = new ProductoBloc();
  final ventasBloc = new VentasBloc();

  int _idOpcion = 0;
  int _idProductoSelected = 0;
  int _currentStep = 0;
  String _fecha = '';
  String _total = '';
  int _cantidadProducto = 10;
  int _cantidadSelected= 1;
  double _precioProducto = 0;
  Map arguments;
  int _idVenta = 0;
  bool _flag;

  Iterable<int> get positiveIntegers sync* {
    int i = 0;
    while (true) yield i++;
  }

  TextEditingController _inputFieldDateController = new TextEditingController();

  @override
  void initState() {
    super.initState();
    _flag = true;
    getOpcionesDropdown(_cantidadProducto);
    Future.delayed(Duration.zero, () async {
      setState(() {
        // {'fecha': venta.fecha, 'id_producto': venta.idProducto, 'iterable': i, 'cantidad':venta.cantidad, 'total':venta.totalVenta }
        arguments = ModalRoute.of(context).settings.arguments as Map;
        _fecha = arguments['fecha'];
      });
      _inputFieldDateController = new TextEditingController(text: _fecha);
      _cantidadSelected = arguments['cantidad'];
      _idVenta = arguments['id_venta'];
    });
  }

   @override
   Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Editar Venta'),
        backgroundColor: Theme.of(context).primaryColor,
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: _crearStepper(context)
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.save, size: 32,),
        isExtended: true,
        backgroundColor: Theme.of(context).primaryColor,
        elevation: 3,
        onPressed: _total.length == 0 ? null : (){
          final venta = VentaModel(idVenta: _idVenta , idProducto: _idProductoSelected, cantidad: _cantidadSelected, totalVenta: double.tryParse(_total), fecha: _fecha);
          
          if(_cantidadSelected > arguments['cantidad']){
            _cantidadSelected = _cantidadSelected - arguments['cantidad'];
            ventasBloc.updateVenta(venta);
            ventasBloc.updateStock(_cantidadSelected, _idProductoSelected);
          }else if(_cantidadSelected < arguments['cantidad']){
            _cantidadSelected = arguments['cantidad'] - _cantidadSelected;
            ventasBloc.updateVenta(venta);
            ventasBloc.updateCancelarVenta(_cantidadSelected, _idProductoSelected);
          }
          Navigator.of(context).pop();
        },
      ),
    );
  }


  List<DropdownMenuItem<int>> getOpcionesDropdown(int cantidad){

    var list = positiveIntegers.skip(1).take(cantidad).toList(); 

    List<DropdownMenuItem<int>> lista = new List();

    list.forEach((numero) {
      lista.add(DropdownMenuItem(
        child: Text(numero.toString()),
        value: numero, 
      ));
    });

    return lista; 

 }

  Widget _crearDropdown(){

    if(_cantidadSelected > _cantidadProducto){
      _cantidadProducto = _cantidadSelected;
    }

    return Row(
      children: <Widget>[
        Icon(Icons.format_list_numbered, color: Theme.of(context).primaryColor,),
        SizedBox(width: 10.0,),
        Expanded(
          child: DropdownButton(
            value: _cantidadSelected,
            items: getOpcionesDropdown(_cantidadProducto),
            onChanged: (opt) {
              setState(() {
                _cantidadSelected = opt;
                getTotalVenta();
              });
            },
          ),
        )

      ],
    );  
  }

  List<DropdownMenuItem<ProductoModel>> getOpcionesDropdownCategoria( List<ProductoModel> productos ){

    List<DropdownMenuItem<ProductoModel>> lista = new List();

    productos.forEach((producto) { 
      lista.add(DropdownMenuItem(
        value: producto,
        child: Text('${producto.nombre}', style: TextStyle(fontSize: 13.3),)
      ));
    });
    return lista; 

  }

  Widget _dropdownProducto(){
     productoBloc.obtenerProductos();
    
      return StreamBuilder<List<ProductoModel>>(
        stream: productoBloc.productosStream,
        builder: (context, AsyncSnapshot<List<ProductoModel>> snapshot){
    
          if ( !snapshot.hasData ){
            return Center(child: CircularProgressIndicator());
          }
          List<ProductoModel> productos = snapshot.data;

          if ( productos.length == 0 ){
            return Center(child: Text('No hay información'));
          }

          if(_flag == true){
            for (var i = 0; i < productos.length; i++) {

              if(arguments['id_producto'] == productos[i].idProducto){
                _idOpcion = i;
              }
              
            }
            _flag = false;
            _idProductoSelected = productos[_idOpcion].idProducto;
            _cantidadProducto = productos[_idOpcion].cantidad;
            _precioProducto = productos[_idOpcion].precioVenta;

          }else{
            _idProductoSelected = productos[_idOpcion].idProducto;
            _cantidadProducto = productos[_idOpcion].cantidad;
            _precioProducto = productos[_idOpcion].precioVenta;
          }

          return Row(
           children: <Widget>[
             Icon(Icons.category, color: Theme.of(context).primaryColor,),
             SizedBox(width: 10.0,),
             Expanded(
               child: DropdownButton(
                 isExpanded: true,
                 value: productos[_idOpcion],
                 items: getOpcionesDropdownCategoria(productos),
                 onChanged: (opt) {
                   for (var i = 0; i < productos.length; i++) {
                     if(opt.idProducto == productos[i].idProducto){
                       _idOpcion = i; 
                     }
                   }
                   setState(() { });
                   _idProductoSelected = productos[_idOpcion].idProducto;
                   _cantidadProducto = productos[_idOpcion].cantidad;
                   _precioProducto = productos[_idOpcion].precioVenta;
                 },
               ),
             )
           ],
         );
        },
      );
    }

  Widget _crearStepper(BuildContext context){
     return Stepper(
          currentStep: _currentStep,
          onStepContinue: _currentStep < 1 && _fecha.length != 0 ? () => setState(() { _currentStep += 1; getTotalVenta();}) : null,
            // _currentStep == 1 && _fecha != '' ? () => setState(() => _currentStep += 1) : null
            // _currentStep < 1 ? () => setState(() => _currentStep += 1) : null
          onStepCancel: _currentStep > 0 ? () => setState(() => _currentStep -= 1) : null,
          steps: <Step>[
            new Step(
              title: new Text('Venta'),
              content: _crearFecha(context),
              isActive: _currentStep >= 0,
              state: _currentStep >= 0 ? StepState.complete : StepState.disabled,
            ),
            new Step(
              title: new Text('Detalle Venta'),
              content: Column(
                children: <Widget>[
                  _dropdownProducto(),
                  Divider(),
                  _crearDropdown(),
                  Divider(),
                  Container(
                    alignment: Alignment.centerLeft,
                    child: Text(r'Total: $'+_total, style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700),),
                  ),
                ],
              ),
              isActive: _currentStep >= 0,
              state: _currentStep >= 1 ? StepState.complete : StepState.disabled,
            ),
          ],
        );
  }

  getTotalVenta(){
    _total = arguments['total'].toString();
    
    if(_total.length <= 5){
      _total = (_precioProducto * _cantidadSelected).toString();
      if(_total.length > 5){
        _total =_total.substring(0,6);
      }
    }else{
      _total = (_precioProducto * _cantidadSelected).toString().substring(0,6);
    }
   
  }

  Widget _crearFecha(BuildContext context){

    return TextField(
      controller: _inputFieldDateController,
      enableInteractiveSelection: false,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15.0)
        ), 
        hintText: 'Fecha de nacimiento',
        labelText: 'Fecha de nacimiento', 
        suffixIcon: Icon(Icons.perm_contact_calendar),
        icon: Icon(Icons.calendar_today)
      ),
      onTap: (){

        FocusScope.of(context).requestFocus(new FocusNode());
        _selectDate(context);

      },
    );

  }

  _selectDate(BuildContext context) async {

      DateTime picked = await showDatePicker(
      context: context, 
      initialDate: new DateTime.now() , 
      firstDate: new DateTime(2000), 
      lastDate: new DateTime(2030),
      locale: Locale('es', 'ES')
    );

    if (picked != null){
      setState(() {
        DateTime _fechaTemp = picked;
        var formatter = new DateFormat('dd/MM/yyyy');
        _fecha = formatter.format(_fechaTemp);
        _inputFieldDateController.text = _fecha;
      });
    }

  }
} 