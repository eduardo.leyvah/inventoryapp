import 'package:flutter/material.dart';
import 'package:inventario_app/src/models/producto_model.dart';


class DetalleProductoPage extends StatefulWidget {

  @override
  _DetalleProductoPageState createState() => _DetalleProductoPageState();
}

class _DetalleProductoPageState extends State<DetalleProductoPage> {
  @override
  Widget build(BuildContext context) {

    final ProductoModel producto = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          _crearAppbar( producto ),
          SliverList(
            delegate: SliverChildListDelegate(
              [
                SizedBox( height: 10.0, ),
                _descripcion( producto ),
              ]
            )
          )
        ],
      ),
    );
  }

  Widget _crearAppbar( ProductoModel producto ){ 

    return SliverAppBar(
      elevation: 3.0,
      expandedHeight: 120.0,
      floating: false,
      pinned: true,
      actions: <Widget>[
        IconButton(
             icon: Icon(Icons.edit), 
             onPressed: () {
               Navigator.pushNamed(context, 'editar_producto', arguments: {'producto':producto, 'id_categoria':producto.catId});
             }
        ),
      ],
      flexibleSpace: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: <Color> [
                Color.fromRGBO(70, 52, 167, 1.0),
                Color.fromRGBO(81, 40, 136, 1.0),
              ]
            )
        ),
        child: FlexibleSpaceBar(
          centerTitle: true,
          title: Text(
            producto.nombre,
            style: TextStyle(
              color: Colors.white,
              fontSize: 16.0,
            ),
            overflow: TextOverflow.ellipsis
          ),
        ),
      ),
    );

  }

  Widget _descripcion( ProductoModel producto ){

    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
          alignment: Alignment.center,
          child: Text(
            'Cantidad: ' + producto.cantidad.toString(),
            textAlign: TextAlign.justify,
            style: TextStyle(fontSize: 15.3, fontWeight: FontWeight.w500),
          ),
        ),
        Divider(),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
          alignment: Alignment.center,
          child: Text(
            r'Precio de Venta: $' +producto.precioVenta.toString(),
            textAlign: TextAlign.justify,
            style: TextStyle(fontSize: 15.3, fontWeight: FontWeight.w500),
          ),
        ),
        Divider(),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
          alignment: Alignment.center,
          child: Text(
            r'Precio de Compra: $' + producto.precioCompra.toString(),
            textAlign: TextAlign.justify,
            style: TextStyle(fontSize: 15.3, fontWeight: FontWeight.w500),
          ),
        ),
        Divider(),
      ],
    );

  }
}