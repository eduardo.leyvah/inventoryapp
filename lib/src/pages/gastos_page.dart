import 'package:flutter/material.dart';
import 'package:inventario_app/src/widgets/card_gastop_widget.dart';
import 'package:inventario_app/src/widgets/card_otrogasto_widget.dart';

class GastosPage extends StatefulWidget {

  @override
  _GastosPageState createState() => _GastosPageState();
}

class _GastosPageState extends State<GastosPage> {
  
  int _gasto;

  @override
  void initState() { 
    super.initState();
    _gasto = 1;
  }

  _setSelectedRadio( int valor ) {

    // prefs.setInt('genero', valor);
    _gasto = valor;
    setState(() {});

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Gastos'),
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: <Color> [
                Color.fromRGBO(70, 52, 167, 1.0),
                Color.fromRGBO(81, 40, 136, 1.0),
              ]
            )
          ),
        ),
        actions: <Widget>[
          IconButton(
             icon: Icon(Icons.add ), 
             onPressed: () {
               if(_gasto == 1){
                 Navigator.pushNamed(context, 'nuevo_gasto');
               }else{
                 Navigator.pushNamed(context, 'nuevo_gasto_otro');
               }
             }
          ),
        ]
      ),
      body: ListView(
        children: <Widget>[
          _crearRadios(),
          Divider(),
          _crearCard()
        ],
      )
    );
  }

  Widget _crearRadios(){
    
   return Row(
     children: <Widget>[
       Expanded(
         child: RadioListTile(
           value: 1, 
           groupValue: _gasto, 
           title: Text('Gasto Productos', style: TextStyle(fontSize: 14.5),),
           onChanged: _setSelectedRadio
         ),
       ),
       Expanded(
         child: RadioListTile(
           value: 2, 
           groupValue: _gasto, 
           title: Text('Otro Gasto', style: TextStyle(fontSize: 14.5)),
           onChanged: _setSelectedRadio
         ),
       ),
     ],
   );
  }

  Widget _crearCard(){
    if(_gasto != 1){
      return CardOtroGastoPage();
    }
    return CardGastoProductoPage();
  }
}