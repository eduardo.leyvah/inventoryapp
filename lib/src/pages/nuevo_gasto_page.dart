import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:inventario_app/src/bloc/gasto_producto_bloc.dart';
import 'package:inventario_app/src/bloc/producto_bloc.dart';
import 'package:inventario_app/src/models/gasto_model.dart';
import 'package:inventario_app/src/models/producto_model.dart';


class NuevoGastoPage extends StatefulWidget {
  @override
   _NuevoGastoPageState createState() => _NuevoGastoPageState();
}
class _NuevoGastoPageState extends State<NuevoGastoPage> {
  
  final productoBloc = new ProductoBloc();
  final gastoBloc = new GastoProductoBloc();

  int _idOpcion = 0;
  int _idProductoSelected = 0;
  int _currentStep = 0;
  String _fecha = '';
  String _total = '';
  int _cantidadSelected = 1;
  double _precioProducto = 0;

  Iterable<int> get positiveIntegers sync* {
    int i = 0;
    while (true) yield i++;
  }

  TextEditingController _inputFieldDateController = new TextEditingController();

   @override
   Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Nueva Gasto Producto'),
        backgroundColor: Theme.of(context).primaryColor,
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: _crearStepper(context)
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.save, size: 32,),
        isExtended: true,
        backgroundColor: Theme.of(context).primaryColor,
        elevation: 3,
        onPressed: _total.length == 0 ? null : (){
          print(_total);
          final gasto = GastoModel(idProducto: _idProductoSelected, cantidad: _cantidadSelected, totalCompra: double.tryParse(_total), fecha: _fecha);
          gastoBloc.agregarGastoP(gasto);
          gastoBloc.updateStockProducto(_cantidadSelected, _idProductoSelected);
          Navigator.of(context).pop();
        },
      ),
    );
  }


 List<DropdownMenuItem<int>> getOpcionesDropdown(){

    var list = positiveIntegers.skip(1).take(100).toList(); 

    List<DropdownMenuItem<int>> lista = new List();

    list.forEach((numero) {
      lista.add(DropdownMenuItem(
        child: Text(numero.toString()),
        value: numero, 
      ));
    });

    return lista; 

 }

  Widget _crearDropdown(){

    return Row(
      children: <Widget>[
        Icon(Icons.format_list_numbered, color: Theme.of(context).primaryColor,),
        SizedBox(width: 10.0,),
        Expanded(
          child: DropdownButton(
            value: _cantidadSelected,
            items: getOpcionesDropdown(),
            onChanged: (opt) {
              setState(() {
                _cantidadSelected = opt;
                getTotalVenta();
              });
            },
          ),
        )

      ],
    );  
  }

  List<DropdownMenuItem<ProductoModel>> getOpcionesDropdownCategoria( List<ProductoModel> productos ){

    List<DropdownMenuItem<ProductoModel>> lista = new List();

    productos.forEach((producto) { 
      lista.add(DropdownMenuItem(
        value: producto,
        child: Text('${producto.nombre}', style: TextStyle(fontSize: 13.3),)
      ));
    });
    return lista; 

  }

   Widget _dropdownProducto(){
     productoBloc.obtenerProductos();
    
      return StreamBuilder<List<ProductoModel>>(
        stream: productoBloc.productosStream,
        builder: (context, AsyncSnapshot<List<ProductoModel>> snapshot){
    
          if ( !snapshot.hasData ){
            return Center(child: CircularProgressIndicator());
          }
          List<ProductoModel> productos = snapshot.data;

          if ( productos.length == 0 ){
            return Center(child: Text('No hay información'));
          }

          _idProductoSelected = productos[_idOpcion].idProducto;
          _precioProducto = productos[_idOpcion].precioCompra;

          return Row(
           children: <Widget>[
             Icon(Icons.category, color: Theme.of(context).primaryColor,),
             SizedBox(width: 10.0,),
             Expanded(
               child: DropdownButton(
                 isExpanded: true,
                 value: productos[_idOpcion],
                 items: getOpcionesDropdownCategoria(productos),
                 onChanged: (opt) {
                   for (var i = 0; i < productos.length; i++) {
                     if(opt.idProducto == productos[i].idProducto){
                       _idOpcion = i; 
                     }
                   }
                   setState(() { });
                   _idProductoSelected = productos[_idOpcion].idProducto;
                   _precioProducto = productos[_idOpcion].precioCompra;
                 },
               ),
             )
           ],
         );
        },
      );
    }

  Widget _crearStepper(BuildContext context){
     return Stepper(
          currentStep: _currentStep,
          onStepContinue: _currentStep < 1 && _fecha.length != 0 ? () => setState(() => _currentStep += 1) : null,
            // _currentStep == 1 && _fecha != '' ? () => setState(() => _currentStep += 1) : null
            // _currentStep < 1 ? () => setState(() => _currentStep += 1) : null
          onStepCancel: _currentStep > 0 ? () => setState(() => _currentStep -= 1) : null,
          steps: <Step>[
            new Step(
              title: new Text('Gasto de Producto'),
              content: _crearFecha(context),
              isActive: _currentStep >= 0,
              state: _currentStep >= 0 ? StepState.complete : StepState.disabled,
            ),
            new Step(
              title: new Text('Detalle Gasto'),
              content: Column(
                children: <Widget>[
                  _dropdownProducto(),
                  Divider(),
                  _crearDropdown(),
                  Divider(),
                  Container(
                    alignment: Alignment.centerLeft,
                    child: Text(r'Total: $'+_total, style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700),),
                  ),
                ],
              ),
              isActive: _currentStep >= 0,
              state: _currentStep >= 1 ? StepState.complete : StepState.disabled,
            ),
          ],
        );
  }

  getTotalVenta(){
    if(_total.length <= 5){
      _total = (_precioProducto * _cantidadSelected).toString();
    }
    _total = (_precioProducto * _cantidadSelected).toString().substring(0,6);
  }

  Widget _crearFecha(BuildContext context){
    
    return TextField(
      controller: _inputFieldDateController,
      enableInteractiveSelection: false,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15.0)
        ), 
        hintText: 'Fecha del gasto',
        labelText: 'Fecha del gasto', 
        suffixIcon: Icon(Icons.perm_contact_calendar),
        icon: Icon(Icons.calendar_today)
      ),
      onTap: (){

        FocusScope.of(context).requestFocus(new FocusNode());
        _selectDate(context);

      },
    );

  }

  _selectDate(BuildContext context) async {

    DateTime picked = await showDatePicker(
      context: context, 
      initialDate: new DateTime.now() , 
      firstDate: new DateTime(2000), 
      lastDate: new DateTime(2030),
      locale: Locale('es', 'ES')
    );

    if (picked != null){
      setState(() {
        DateTime _fechaTemp = picked;
        var formatter = new DateFormat('dd/MM/yyyy');
        _fecha = formatter.format(_fechaTemp);
        _inputFieldDateController.text = _fecha;
      });
    }

  }
} 