import 'package:flutter/material.dart';

//import 'package:charts_flutter/flutter.dart' as charts;
// import 'package:inventario_app/src/bloc/venta_bloc.dart';
import 'package:inventario_app/src/pages/gastos_page.dart';
import 'package:inventario_app/src/pages/ventas_page.dart';
import 'package:inventario_app/src/providers/db_provider.dart';


class DashboardPage extends StatefulWidget {
  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {

  int currentIndex = 0;
  String _ventas = '';
  String _gastos = '';
  String _ganancias = '';
  // List<VentaModel> _res;
  double _totalVentas = 0;
  double _totalGastos = 0;
  double _totalOtrosGastos = 0;
  double _gastosTodos = 0;
  // final ventasBloc = VentasBloc();
  List<VentaModel> _resVenta;
  List<GastoModel> _resGasto;
  List<OtroGastoModel> _resOtroGasto;

  @override
  void initState() {
   super.initState();
   getVentas();
   getGastos();
   getOtrosGastos();
  //  ventasBloc.obtenerVenta();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:  _callPage(context, currentIndex),
      bottomNavigationBar: _bottomNavigationBar(context),
    );
  }

  Widget _crearDashboard(BuildContext context){
    getVentas().then((val) => setState(() {
      _resVenta = val;
    }));
    getGastos().then((val) => setState(() {
      _resGasto = val;
    }));
    getOtrosGastos().then((val) => setState(() {
      _resOtroGasto = val;
    }));
    return Container(
        child: Stack(
          children: <Widget>[
            _crearFondo(),
            _crearTitulo(context),
            _crearGanancias(context),
            _crearFondoBlanco(context)
          ],
        ),
    );
  }

  Widget _callPage(BuildContext context, int paginaActual){

    switch ( paginaActual ) {

      case 0: return _crearDashboard(context);
      case 1: return VentasPage();
      case 2: return GastosPage();

      default:
        return _crearDashboard(context);

    }

  }

  Widget _bottomNavigationBar(BuildContext context){

    return Theme(
      data: Theme.of(context).copyWith(
        primaryColor: Color.fromRGBO(81, 40, 136, 1.0),
        textTheme: Theme.of(context).textTheme
        .copyWith( caption: TextStyle(color: Color.fromRGBO(116, 117, 152, 1.0)) )
      ),
      child: BottomNavigationBar(
        currentIndex: currentIndex,
        onTap: (index) {
          setState(() {
            currentIndex = index;
          });
        },
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.dashboard, size: 25.0),
            title: Text('Dashboard')
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.insert_chart, size: 25.0),
            title: Text('Ventas')
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.monetization_on, size: 25.0),
            title: Text('Gastos')
          ),
        ]

      ),
    );

  }

  Widget  _crearTitulo(BuildContext context) {

    return SafeArea(
      child: Row(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(top: 17),
            alignment: Alignment.topCenter,
            child: IconButton(
              icon: Icon(Icons.keyboard_arrow_left,size: 40.0,color: Colors.white), 
              onPressed: () => Navigator.pushReplacementNamed(context, 'home')
            )
          ),
          SizedBox(width: 15,),
          Container(
            padding: EdgeInsets.symmetric(vertical: 25.0),
            alignment: Alignment.topCenter,
            child: Text(
              'Finanzas',
              style: TextStyle(
                fontSize: 30.0,
                color: Colors.white,
                fontWeight: FontWeight.bold,
                shadows: <Shadow>[
                  Shadow(
                    offset: Offset(4.0, 4.0),
                    blurRadius: 5.0,
                    color: Colors.black87
                  ),
                  Shadow(
                    offset: Offset(4.0, 4.0),
                    blurRadius: 8.0,
                    color: Color.fromARGB(125, 0, 0, 255),
                  ),
                ],
              ),
            ),
          ),
        ],
      )
    );

  }

  Widget _crearFondo(){
    return Container(
      width: double.infinity,
      height: double.infinity,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: <Color> [
            Color.fromRGBO(70, 52, 167, 1.0),
            Color.fromRGBO(81, 40, 136, 1.0),
          ]
        )
      ),
    );
  }

  Widget _crearGanancias(BuildContext context){

    final size = MediaQuery.of(context).size;

     return Container(
          padding: size.width > 320 ? EdgeInsets.symmetric(horizontal:112, vertical: 90) : EdgeInsets.all(85) ,
          child: Column(
            children: <Widget>[
              SizedBox(height: 20,),
              Text(
                'Ganancias', 
                style: TextStyle(
                  fontSize: 28, 
                  color: Colors.white, 
                  fontWeight: FontWeight.bold,
                  shadows: <Shadow>[
                    Shadow(
                      offset: Offset(4.0, 4.0),
                      blurRadius: 5.0,
                      color: Colors.black87
                    ),
                    Shadow(
                      offset: Offset(4.0, 4.0),
                      blurRadius: 8.0,
                      color: Color.fromARGB(125, 0, 0, 255),
                    ),
                  ]
                ),
              ),
              SizedBox(height: 10,),
              Text(
                r'$'+getGanancias(), 
                style: TextStyle(
                  fontSize: 28, 
                  color: Colors.white, 
                  fontWeight: FontWeight.bold,
                  shadows: <Shadow>[
                    Shadow(
                      offset: Offset(4.0, 4.0),
                      blurRadius: 5.0,
                      color: Colors.black87
                    ),
                    Shadow(
                      offset: Offset(4.0, 4.0),
                      blurRadius: 8.0,
                      color: Color.fromARGB(125, 0, 0, 255),
                    ),
                  ]
                ),
              ),
            ],
          )
     );
   
    
  }

  Future<List<VentaModel>> getVentas() async {
    final db = DBProvider.db;
    _resVenta = await db.getTodasVentas(); 
    return _resVenta;
  }

  Future<List<GastoModel>> getGastos() async {
    final db = DBProvider.db;
    _resGasto = await db.getTodosGastosP(); 
    return _resGasto;
  }

  Future<List<OtroGastoModel>> getOtrosGastos() async {
    final db = DBProvider.db;
    _resOtroGasto = await db.getTodosGastosOtros(); 
    return _resOtroGasto;
  }

  String getTotal() {
    _totalVentas = 0;
    
    if(_resVenta == null){
      getVentas().then((val) => setState(() {
        _resVenta = val;
      }));
    }else{
      for (var i = 0; i < _resVenta.length; i++) {
            _totalVentas = _totalVentas + _resVenta[i].totalVenta;
      }
    }

    if(_totalVentas < 10000){
      if( _totalVentas.toString().length <= 5){
        _ventas = _totalVentas.toString();
        return _ventas;
      }else{
        _ventas = _totalVentas.toString().substring(0,6);
        return _ventas;
      }
    }else{
      if( _totalVentas.toString().length <= 5){
        _ventas = _totalVentas.toString();
        return _ventas;
      }else{
        _ventas = _totalVentas.toString().substring(0,7);
        return _ventas;
      }
    }

    
  
  }

  String getTotalGastos(){

    _totalGastos = 0;
    _totalOtrosGastos = 0;
    _gastosTodos = 0;

    if(_resGasto == null || _resOtroGasto == null){
      getGastos().then((val) => setState(() {
        _resGasto = val;
      }));
      getOtrosGastos().then((val) => setState(() {
        _resOtroGasto = val;
      }));
    } else{
      for (var i = 0; i < _resGasto.length; i++) {
            _totalGastos = _totalGastos + _resGasto[i].totalCompra;
      }
      for (var i = 0; i < _resOtroGasto.length; i++) {
            _totalOtrosGastos = _totalOtrosGastos + _resOtroGasto[i].totalGasto;
      }
      _gastosTodos = _totalGastos + _totalOtrosGastos;
    }

    if(_gastosTodos < 10000){
      if( _gastosTodos.toString().length <= 5){
        _gastos = _gastosTodos.toString();
        return _gastos;
      }else{
        _gastos = _gastosTodos.toString().substring(0,6);
        return _gastos;
      }
    }else{
      if( _totalVentas.toString().length <= 5){
        _gastos = _gastosTodos.toString();
        return _gastos;
      }else{
        _gastos = _gastosTodos.toString().substring(0,7);
        return _gastos;
      }
    }

  }

  String getGanancias(){
    getTotal();
    getTotalGastos();

    final double ganancia = double.tryParse(_ventas) - double.tryParse(_gastos);

    if(ganancia < 10000){
      _ganancias = ganancia.toString();

      if( _ganancias.length <= 5){
        return _ganancias;
      }else{
        _ganancias = _ganancias.substring(0,6);
        return _ganancias;
      }
    }else{
      _ganancias = ganancia.toString();

      if( _ganancias.length <= 5){
        return _ganancias;
      }else{
        _ganancias = _ganancias.substring(0,7);
        return _ganancias;
      }
    }

  }


  Widget _crearCardDashboard( Color backColor, Color backGradient, String texto, String ventas) {

    return Container(
          height: 100.0,
          width: 200.0,
          margin: EdgeInsets.symmetric(horizontal: 8.0, vertical: 15.0),
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.bottomLeft,
              end: Alignment.topRight,
              colors: [
                backColor,
                backGradient,
              ]
            ),
            // color: backColor,
            borderRadius: BorderRadius.circular(20.0)
          ),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(20.0),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  SizedBox( height: 5.0 ),
                  Text(texto, style: TextStyle(fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold )),
                  Text( r"$"+ventas, style: TextStyle(fontSize: 18, color: Colors.white , fontWeight: FontWeight.w600 )),
                  SizedBox( height: 5.0 )
                ],
              ),
          ),

        );
  }

  Widget _crearFondoBlanco(BuildContext context){

    final size = MediaQuery.of(context).size;

    final fondo = Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        Container(
          height: size.width > 320 ? size.height * 0.55 : size.height * 0.48,
          width: double.infinity,
          decoration: BoxDecoration(
            // borderRadius: size.width < 412 ? BorderRadius.only(topLeft: Radius.circular(45.0), topRight: Radius.circular(45.0))
            //                                : BorderRadius.only(topLeft: Radius.circular(40.0), topRight: Radius.circular(40.0)),
            //borderRadius: BorderRadius.only(topLeft: Radius.circular(40.0), topRight: Radius.circular(40.0)),
            color: Colors.white,
            boxShadow: <BoxShadow>[
                    BoxShadow(
                      color: Colors.black38,
                      blurRadius: 5.0,
                      offset: Offset(0.0, - 5.0),
                      spreadRadius: 4.0
                    )
            ]
          ),
        ),
      ],
    );

    return Stack(
      children: <Widget>[
        fondo,
        _crearCards2(context)
      ],
    );

  }

  Widget _crearCards2(BuildContext context){

    final size = MediaQuery.of(context).size;

    return Container(
          padding: size.width > 320 ? EdgeInsets.only(left: 70, bottom: 50) : EdgeInsets.only(left: 50, bottom: 10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              _crearCardDashboard( Color.fromRGBO(0, 212, 14, 1.0), Color.fromRGBO(0, 150, 88, 1.0), 'Ventas', getTotal()),
              _crearCardDashboard( Color.fromRGBO(251, 41, 27, 1.0), Color.fromRGBO(176, 17, 53, 1.0), 'Gastos', getTotalGastos()),
            ],
          )
        );
   
    
  }



}