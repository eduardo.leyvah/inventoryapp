import 'package:flutter/material.dart';

import 'package:extended_text_editing_controllers/double_text_editing_controller.dart';
import 'package:extended_text_editing_controllers/plugin.dart';
import 'package:intl/intl.dart';

import 'package:inventario_app/src/bloc/otro_gasto_bloc.dart';
import 'package:inventario_app/src/providers/db_provider.dart';


 class EditarOtroGastoPage extends StatefulWidget {
   @override
    _EditarOtroGastoPageState createState() => _EditarOtroGastoPageState();
 }
 class _EditarOtroGastoPageState extends State<EditarOtroGastoPage> {

   final otroGastoBloc = new OtrosGastosBloc();

   TextEditingController _controllerInput;
   DoubleTextEditingController _controllerTotal;
   TextEditingController _inputFieldDateController = new TextEditingController();

  String _fecha = '';
  String _total = '';
  String _concepto = '';
  int _currentStep = 0;
  Map arguments;
  int _idOtroGasto = 0;

  iniciarControllers(String concepto, String total, String fecha) async {
    _controllerInput = new TextEditingController(text: concepto);
    _controllerTotal = new DoubleTextEditingController(text: total);
   _inputFieldDateController = new TextEditingController(text: fecha);
  }

   @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () async {
      setState(() {
        arguments = ModalRoute.of(context).settings.arguments as Map;
        _fecha = arguments['fecha'];
        _total = arguments['total'].toString();
        _concepto = arguments['concepto'];
      });
      iniciarControllers(_concepto, _total, _fecha);
      _idOtroGasto = arguments['id_gasto'];
    });
  }

    @override
    Widget build(BuildContext context) {
     return Scaffold(
       appBar: AppBar(
         title: Text('Editar Otro Gasto'),
         backgroundColor: Theme.of(context).primaryColor,
       ),
       body: SingleChildScrollView(
         scrollDirection: Axis.vertical,
         child: _crearStepper(context)
       ),
       floatingActionButton: FloatingActionButton(
         child: Icon(Icons.save, size: 32,),
         isExtended: true,
         backgroundColor: Theme.of(context).primaryColor,
         elevation: 3,
         onPressed: _total.length == 0 ? null : (){
          final gasto = OtroGastoModel(idGastoOtro: _idOtroGasto, concepto: _concepto, fecha: _fecha, totalGasto: double.tryParse(_total));
          otroGastoBloc.updateOtroGasto(gasto);
           Navigator.of(context).pop();
         },
       ),
     );
   }
   

    Widget _crearInputTotal() {
  
    return TextField(
      //autofocus: true,
      controller: _controllerTotal,
      textCapitalization: TextCapitalization.sentences,
      keyboardType: TextInputType.numberWithOptions(decimal: true),
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15.0)
        ),
        hintText: 'Total',
        labelText: 'Total',
        icon: Icon(Icons.monetization_on)
      ),
      onChanged: (valor) => setState(() {
          _total = valor;
      })
    );
  }

  Widget _crearStepper(BuildContext context){
     return Stepper(
          currentStep: _currentStep,
          onStepContinue: _currentStep < 1 && _fecha.length != 0 ? () => setState(() => _currentStep += 1) : null,
            // _currentStep == 1 && _fecha != '' ? () => setState(() => _currentStep += 1) : null
            // _currentStep < 1 ? () => setState(() => _currentStep += 1) : null
          onStepCancel: _currentStep > 0 ? () => setState(() => _currentStep -= 1) : null,
          steps: <Step>[
            new Step(
              title: new Text('Otro Gasto'),
              content: _crearFecha(context),
              isActive: _currentStep >= 0,
              state: _currentStep >= 0 ? StepState.complete : StepState.disabled,
            ),
            new Step(
              title: new Text('Detalle Gasto'),
              content: Column(
                children: <Widget>[
                  _crearInputConcepto(),
                  Divider(),
                  _crearInputTotal(),
                  Divider(),
                ],
              ),
              isActive: _currentStep >= 0,
              state: _currentStep >= 1 ? StepState.complete : StepState.disabled,
            ),
          ],
        );
  }

  Widget _crearInputConcepto() {
  
    return TextField(
      //autofocus: true,
      controller: _controllerInput,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15.0)
        ),
        hintText: 'Concepto del gasto',
        labelText: 'Concepto',
        icon: Icon(Icons.archive)
      ),
      onChanged: (valor) => setState(() {
          _concepto = valor;
      })
    );
  }

  Widget _crearFecha(BuildContext context){
    
    return TextField(
      controller: _inputFieldDateController,
      enableInteractiveSelection: false,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15.0)
        ), 
        hintText: 'Fecha del gasto',
        labelText: 'Fecha del gasto', 
        suffixIcon: Icon(Icons.perm_contact_calendar),
        icon: Icon(Icons.calendar_today)
      ),
      onTap: (){

        FocusScope.of(context).requestFocus(new FocusNode());
        _selectDate(context);

      },
    );

  }

  _selectDate(BuildContext context) async {

    DateTime picked = await showDatePicker(
      context: context, 
      initialDate: new DateTime.now() , 
      firstDate: new DateTime(2000), 
      lastDate: new DateTime(2030),
      locale: Locale('es', 'ES')
    );

    if (picked != null){
      setState(() {
        DateTime _fechaTemp = picked;
        var formatter = new DateFormat('dd/MM/yyyy');
        _fecha = formatter.format(_fechaTemp);
        _inputFieldDateController.text = _fecha;
      });
    }

  }

 } 