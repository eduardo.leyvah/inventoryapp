import 'package:flutter/material.dart';

import 'package:extended_text_editing_controllers/double_text_editing_controller.dart';
import 'package:extended_text_editing_controllers/plugin.dart';

import 'package:inventario_app/src/bloc/categoria_bloc.dart';
import 'package:inventario_app/src/bloc/producto_bloc.dart';
import 'package:inventario_app/src/models/categoria_model.dart';
import 'package:inventario_app/src/models/producto_model.dart';
import 'package:inventario_app/src/providers/db_provider.dart';

class EditarProductoPage extends StatefulWidget {
  @override
   _EditarProductoPageState createState() => _EditarProductoPageState();
}

class _EditarProductoPageState extends State<EditarProductoPage> {
  
  final categoriaBloc = new CategoriaBloc();
  final productoBloc = new ProductoBloc();
  final db = DBProvider.db;
  
  int _idOpcion = 0;
  int _idCategoriaSelected = 0;
  // String _precioVenta = '';
  
  ProductoModel argumentProducto;
  TextEditingController _controllerInput;
  DoubleTextEditingController _controllerPrecioVenta;
  DoubleTextEditingController _controllerPrecioCompra;
  Map arguments;
  bool _flag;

  Iterable<int> get positiveIntegers sync* {
    int i = 0;
    while (true) yield i++;
  }

  iniciarControllers(argumentProducto) async {
    _controllerInput = new TextEditingController(text: argumentProducto.nombre);
    _controllerPrecioVenta = new DoubleTextEditingController(text: argumentProducto.precioVenta.toString());
    _controllerPrecioCompra = new DoubleTextEditingController(text:  argumentProducto.precioCompra.toString());
    _crearDropdown();
  }

  @override
  void initState() {
    super.initState();
    _flag = true;
    Future.delayed(Duration.zero, () async {
      setState(() {
        arguments = ModalRoute.of(context).settings.arguments as Map;
        argumentProducto = arguments['producto'];
      });
      iniciarControllers(argumentProducto);
    });
  }

   @override
   Widget build(BuildContext context) {
    
    return Scaffold(
       appBar: AppBar(
         title: Text('Editar Producto'),
         backgroundColor: Theme.of(context).primaryColor,
       ),
       body: SingleChildScrollView(
         scrollDirection: Axis.vertical,
         child: Container(
           padding: EdgeInsets.all(20),
           child: Column(
             children: <Widget>[
               _crearInputProducto(),
               Divider(),
               _dropdownCategoria(),
               Divider(),
               _crearInputPrecioVenta(),
               Divider(),
               _crearInputPrecioCompra(),
               Divider(),
               _crearDropdown(),
               Divider(),
               Container(
                 child: MaterialButton(
                   padding: EdgeInsets.symmetric(horizontal: 30, vertical: 13.5),
                   elevation: 3,
                   color: Theme.of(context).primaryColor,
                   child: Text('Aceptar'),
                   textColor: Colors.white,
                   shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0),
                   ),
                   onPressed: () {
                     final producto = ProductoModel(idProducto: argumentProducto.idProducto, nombre: argumentProducto.nombre, catId: _idCategoriaSelected, precioVenta: argumentProducto.precioVenta, precioCompra: argumentProducto.precioCompra, cantidad: argumentProducto.cantidad);
                     productoBloc.updateProducto(producto);
                     Navigator.of(context).pop();
                   },
                 ),
               )
             ],
           ),
         ),
       ),
    );
  }

  Widget _crearInputProducto() {
  
    return TextField(
      //autofocus: true,
      controller: _controllerInput,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15.0)
        ),
        hintText: 'Nombre Producto',
        labelText: 'Producto',
        icon: Icon(Icons.archive)
      ),
      onChanged: (valor) => setState(() {
          argumentProducto.nombre = valor;
      })
    );
  }

  List<DropdownMenuItem<CategoriaModel>> getOpcionesDropdownCategoria( List<CategoriaModel> categorias ){

    List<DropdownMenuItem<CategoriaModel>> lista = new List();

    categorias.forEach((categoria) { 
      lista.add(DropdownMenuItem(
        value: categoria,
        child: Text('${categoria.nombre}', style: TextStyle(fontSize: 13.3),)
      ));
    });
    return lista; 

  }

  Widget _dropdownCategoria(){
    categoriaBloc.obtenerCategorias();
    
     return StreamBuilder<List<CategoriaModel>>(
       stream: categoriaBloc.categoriasStream,
       builder: (context, AsyncSnapshot<List<CategoriaModel>> snapshot){
    
         if ( !snapshot.hasData ){
           return Center(child: CircularProgressIndicator());
         }
         List<CategoriaModel> categorias = snapshot.data;
    
         if ( categorias.length == 0 ){
           return Center(child: Text('No hay información'));
         }

         if(_flag == true){
            for (var i = 0; i < categorias.length; i++) {

                if(arguments['id_categoria'] == categorias[i].idCategoria){
                  _idOpcion = i;
                }
                
            }
            _idCategoriaSelected = categorias[_idOpcion].idCategoria;
            _flag = false;
         }else{
            _idCategoriaSelected = categorias[_idOpcion].idCategoria;
         }

         return Row(
          children: <Widget>[
            Icon(Icons.category, color: Theme.of(context).primaryColor,),
            SizedBox(width: 10.0,),
            Expanded(
              child: DropdownButton(
                isExpanded: true,
                value: categorias[_idOpcion],
                items: getOpcionesDropdownCategoria(categorias),
                onChanged: (opt) {
                  for (var i = 0; i < categorias.length; i++) {
                    if(opt.idCategoria == categorias[i].idCategoria){
                      _idOpcion = i; 
                    }
                  }
                  setState(() { });
                  _idCategoriaSelected = categorias[_idOpcion].idCategoria;
                },
              ),
            )
          ],
        );
       },
     );
   }

  Widget _crearInputPrecioVenta() {
  
    return TextField(
      //autofocus: true,
      controller: _controllerPrecioVenta,
      keyboardType: TextInputType.numberWithOptions(decimal: true),
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15.0)
        ),
        hintText: 'Precio de Venta',
        labelText: 'Precio de Venta',
        icon: Icon(Icons.monetization_on)
      ),
      onChanged: (valor) { 
        setState(() {
          _controllerPrecioVenta.text = valor;
        });
        argumentProducto.precioVenta = double.tryParse(_controllerPrecioVenta.text);
      }
    );
  }

  Widget _crearInputPrecioCompra() {
  
    return TextField(
      //autofocus: true,
      controller: _controllerPrecioCompra,
      keyboardType: TextInputType.numberWithOptions(decimal: true),
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15.0)
        ),
        hintText: 'Precio de Compra',
        labelText: 'Precio de Compra',
        icon: Icon(Icons.monetization_on)
      ),
      onChanged: (valor) { 
        setState(() {
          _controllerPrecioCompra.text = valor;
        });
        argumentProducto.precioCompra = double.tryParse(_controllerPrecioCompra.text);
      }
    );
  }

 List<DropdownMenuItem<int>> getOpcionesDropdown(){

    var list = positiveIntegers.skip(1).take(100).toList(); 

    List<DropdownMenuItem<int>> lista = new List();

    list.forEach((numero) {
      lista.add(DropdownMenuItem(
        child: Text(numero.toString()),
        value: numero, 
      ));
    });

    return lista; 

 }

 Widget _crearDropdown(){

    return Row(
      children: <Widget>[
        Icon(Icons.format_list_numbered, color: Theme.of(context).primaryColor,),
        SizedBox(width: 10.0,),
        Expanded(
          child: DropdownButton(
            value: argumentProducto.cantidad,
            items: getOpcionesDropdown(),
            onChanged: (opt) {
              setState(() {
                argumentProducto.cantidad = opt;
              });
            },
          ),
        )

      ],
    );  
  }
} 