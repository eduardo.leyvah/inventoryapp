import 'package:flutter/material.dart';

import 'package:inventario_app/src/widgets/fondo_widget.dart';
import 'package:inventario_app/src/widgets/login_widget.dart';


class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Stack(
        children: <Widget>[
          _crearFondo(),
          _crearLogin()
        ],
      )
    );
  }

  Widget _crearLogin(){
    return LoginForm();
  }

  Widget _crearFondo(){
    return FondoWidget();
  }


}