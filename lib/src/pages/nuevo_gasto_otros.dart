import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:inventario_app/src/bloc/otro_gasto_bloc.dart';
import 'package:inventario_app/src/models/otro_gasto_model.dart';


class GastoOtroPage extends StatefulWidget {
  @override
   _GastoOtroPageState createState() => _GastoOtroPageState();
}
class _GastoOtroPageState extends State<GastoOtroPage> {

  final otroGastoBloc = new OtrosGastosBloc();

  String _fecha = '';
  double _total = 0;
  String _concepto = '';
  int _currentStep = 0;

  TextEditingController _inputFieldDateController = new TextEditingController();

   @override
   Widget build(BuildContext context) {
    return Scaffold(
       appBar: AppBar(
        title: Text('Nuevo Gasto Otro'),
        backgroundColor: Theme.of(context).primaryColor,
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: _crearStepper(context)
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.save, size: 32,),
        isExtended: true,
        backgroundColor: Theme.of(context).primaryColor,
        elevation: 3,
        onPressed: _total == 0 ? null : (){
          // print(_total);
          final gasto = OtroGastoModel(fecha: _fecha, concepto: _concepto, totalGasto: _total);
          otroGastoBloc.agregarOtroGasto(gasto);
          // gastoBloc.agregarGastoP(gasto);
          // gastoBloc.updateStockProducto(_cantidadSelected, _idProductoSelected);
          Navigator.of(context).pop();
        },
      ),
    );
  }

  Widget _crearStepper(BuildContext context){
     return Stepper(
          currentStep: _currentStep,
          onStepContinue: _currentStep < 1 && _fecha.length != 0 ? () => setState(() => _currentStep += 1) : null,
            // _currentStep == 1 && _fecha != '' ? () => setState(() => _currentStep += 1) : null
            // _currentStep < 1 ? () => setState(() => _currentStep += 1) : null
          onStepCancel: _currentStep > 0 ? () => setState(() => _currentStep -= 1) : null,
          steps: <Step>[
            new Step(
              title: new Text('Otro Gasto'),
              content: _crearFecha(context),
              isActive: _currentStep >= 0,
              state: _currentStep >= 0 ? StepState.complete : StepState.disabled,
            ),
            new Step(
              title: new Text('Detalle Gasto'),
              content: Column(
                children: <Widget>[
                  _crearInputConcepto(),
                  Divider(),
                  _crearInputTotal(),
                  Divider(),
                ],
              ),
              isActive: _currentStep >= 0,
              state: _currentStep >= 1 ? StepState.complete : StepState.disabled,
            ),
          ],
        );
  }

  Widget _crearInputTotal() {
  
    return TextField(
      //autofocus: true,
      textCapitalization: TextCapitalization.sentences,
      keyboardType: TextInputType.numberWithOptions(decimal: true),
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15.0)
        ),
        hintText: 'Total',
        labelText: 'Total',
        icon: Icon(Icons.monetization_on)
      ),
      onChanged: (valor) => setState(() {
          _total = double.tryParse(valor);
      })
    );
  }

  Widget _crearInputConcepto() {
  
    return TextField(
      //autofocus: true,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15.0)
        ),
        hintText: 'Concepto del gasto',
        labelText: 'Concepto',
        icon: Icon(Icons.archive)
      ),
      onChanged: (valor) => setState(() {
          _concepto = valor;
      })
    );
  }

  Widget _crearFecha(BuildContext context){
    
    return TextField(
      controller: _inputFieldDateController,
      enableInteractiveSelection: false,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15.0)
        ), 
        hintText: 'Fecha del gasto',
        labelText: 'Fecha del gasto', 
        suffixIcon: Icon(Icons.perm_contact_calendar),
        icon: Icon(Icons.calendar_today)
      ),
      onTap: (){

        FocusScope.of(context).requestFocus(new FocusNode());
        _selectDate(context);

      },
    );

  }

  _selectDate(BuildContext context) async {

    DateTime picked = await showDatePicker(
      context: context, 
      initialDate: new DateTime.now() , 
      firstDate: new DateTime(2000), 
      lastDate: new DateTime(2030),
      locale: Locale('es', 'ES')
    );

    if (picked != null){
      setState(() {
        DateTime _fechaTemp = picked;
        var formatter = new DateFormat('dd/MM/yyyy');
        _fecha = formatter.format(_fechaTemp);
        _inputFieldDateController.text = _fecha;
      });
    }

  }
} 