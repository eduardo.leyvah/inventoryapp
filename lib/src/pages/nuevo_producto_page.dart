import 'package:flutter/material.dart';

import 'package:inventario_app/src/bloc/categoria_bloc.dart';
import 'package:inventario_app/src/bloc/producto_bloc.dart';
import 'package:inventario_app/src/models/categoria_model.dart';
import 'package:inventario_app/src/models/producto_model.dart';


class NuevoProductoPage extends StatefulWidget {
  @override
   _NuevoProductoPageState createState() => _NuevoProductoPageState();
}
class _NuevoProductoPageState extends State<NuevoProductoPage> {

  final categoriaBloc = new CategoriaBloc();
  final productoBloc = new ProductoBloc();
  

  String _nombreProducto = '';
  int _idOpcion = 0;
  int _idCategoriaSelected = 0;
  double _precioVenta = 0.0;
  double _precioCompra = 0.0;
  int _cantidadSeleccionada = 1;

  Iterable<int> get positiveIntegers sync* {
    int i = 0;
    while (true) yield i++;
  }

   @override
   Widget build(BuildContext context) {
    return Scaffold(
       appBar: AppBar(
         title: Text('Nuevo Producto'),
         backgroundColor: Theme.of(context).primaryColor,
       ),
       body: SingleChildScrollView(
         scrollDirection: Axis.vertical,
         child: Container(
           padding: EdgeInsets.all(20),
           child: Column(
             children: <Widget>[
               _crearInputProducto(),
               Divider(),
               _dropdownCategoria(),
               Divider(),
               _crearInputPrecioVenta(),
               Divider(),
               _crearInputPrecioCompra(),
               Divider(),
               _crearDropdown(),
               Divider(),
               Container(
                 child: MaterialButton(
                   padding: EdgeInsets.symmetric(horizontal: 30, vertical: 13.5),
                   elevation: 3,
                   color: Theme.of(context).primaryColor,
                   child: Text('Agregar'),
                   textColor: Colors.white,
                   shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0),
                   ),
                   onPressed: () {
                     final producto = ProductoModel(nombre: _nombreProducto, catId: _idCategoriaSelected, precioVenta: _precioVenta, precioCompra: _precioCompra, cantidad: _cantidadSeleccionada);
                     productoBloc.agregarProducto(producto);
                     Navigator.of(context).pop();
                   },
                 ),
               )
             ],
           ),
         ),
       ),
    );
  }

  Widget _crearInputProducto() {
  
    return TextField(
      //autofocus: true,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15.0)
        ),
        hintText: 'Nombre del producto',
        labelText: 'Producto',
        icon: Icon(Icons.archive)
      ),
      onChanged: (valor) => setState(() {
          _nombreProducto = valor;
      })
    );
  }

  List<DropdownMenuItem<CategoriaModel>> getOpcionesDropdownCategoria( List<CategoriaModel> categorias ){

    List<DropdownMenuItem<CategoriaModel>> lista = new List();

    categorias.forEach((categoria) { 
      lista.add(DropdownMenuItem(
        value: categoria,
        child: Text('${categoria.nombre}', style: TextStyle(fontSize: 13.3),)
      ));
    });
    return lista; 

  }

  Widget _dropdownCategoria(){
    categoriaBloc.obtenerCategorias();
    
     return StreamBuilder<List<CategoriaModel>>(
       stream: categoriaBloc.categoriasStream,
       builder: (context, AsyncSnapshot<List<CategoriaModel>> snapshot){
    
         if ( !snapshot.hasData ){
           return Center(child: CircularProgressIndicator());
         }
         List<CategoriaModel> categorias = snapshot.data;
    
         if ( categorias.length == 0 ){
           return Center(child: Text('No hay información'));
         }

         _idCategoriaSelected = categorias[_idOpcion].idCategoria;

         return Row(
          children: <Widget>[
            Icon(Icons.category, color: Theme.of(context).primaryColor,),
            SizedBox(width: 10.0,),
            Expanded(
              child: DropdownButton(
                isExpanded: true,
                value: categorias[_idOpcion],
                items: getOpcionesDropdownCategoria(categorias),
                onChanged: (opt) {
                  for (var i = 0; i < categorias.length; i++) {
                    if(opt.idCategoria == categorias[i].idCategoria){
                      _idOpcion = i; 
                    }
                  }
                  setState(() { });
                  _idCategoriaSelected = categorias[_idOpcion].idCategoria;
                },
              ),
            )
          ],
        );
       },
     );
   }

 Widget _crearInputPrecioVenta() {
  
    return TextField(
      //autofocus: true,
      textCapitalization: TextCapitalization.sentences,
      keyboardType: TextInputType.numberWithOptions(decimal: true),
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15.0)
        ),
        hintText: 'Precio de venta',
        labelText: 'Precio de venta',
        icon: Icon(Icons.monetization_on)
      ),
      onChanged: (valor) => setState(() {
          _precioVenta = double.tryParse(valor);
      })
    );
  }

 Widget _crearInputPrecioCompra() {
  
    return TextField(
      //autofocus: true,
      textCapitalization: TextCapitalization.sentences,
      keyboardType: TextInputType.numberWithOptions(decimal: true),
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15.0)
        ),
        hintText: 'Precio de compra',
        labelText: 'Precio de compra',
        icon: Icon(Icons.monetization_on)
      ),
      onChanged: (valor) => setState(() {
          _precioCompra = double.tryParse(valor);
      })
    );
  }

 List<DropdownMenuItem<int>> getOpcionesDropdown(){

    var list = positiveIntegers.skip(1).take(100).toList(); 

    List<DropdownMenuItem<int>> lista = new List();

    list.forEach((numero) {
      lista.add(DropdownMenuItem(
        child: Text(numero.toString()),
        value: numero, 
      ));
    });

    return lista; 

 }

 Widget _crearDropdown(){

    return Row(
      children: <Widget>[
        Icon(Icons.format_list_numbered, color: Theme.of(context).primaryColor,),
        SizedBox(width: 10.0,),
        Expanded(
          child: DropdownButton(
            value: _cantidadSeleccionada,
            items: getOpcionesDropdown(),
            onChanged: (opt) {
              setState(() {
                _cantidadSeleccionada = opt;
              });
            },
          ),
        )

      ],
    );  
  }

} 