import 'package:flutter/material.dart';
import 'package:inventario_app/src/pages/categorias_page.dart';
import 'package:inventario_app/src/pages/productos_page.dart';
import 'package:inventario_app/src/widgets/drawer_widget.dart';


class HomePage extends StatefulWidget {
  
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _callPage(context, currentIndex),
      drawer: _crearDrawer(),
      bottomNavigationBar: _bottomNavigationBar(context)
    );
  }

  Widget _callPage(BuildContext context, int paginaActual){

    switch ( paginaActual ) {

      case 0: return CategoriasPage();
      case 1: return ProductosPage();

      default:
        return CategoriasPage();

    }

  }

  Widget _crearDrawer() {
    return DrawerWidget();
  }

  Widget _bottomNavigationBar(BuildContext context){

    return Theme(
      data: Theme.of(context).copyWith(
        primaryColor: Color.fromRGBO(81, 40, 136, 1.0),
        textTheme: Theme.of(context).textTheme
        .copyWith( caption: TextStyle(color: Color.fromRGBO(116, 117, 152, 1.0)) )
      ),
      child: BottomNavigationBar(
        currentIndex: currentIndex,
        onTap: (index) {
          setState(() {
            currentIndex = index;
          });
        },
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.category, size: 25.0),
            title: Text('Categorías')
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.archive, size: 25.0),
            title: Text('Inventario')
          ),
        ]

      ),
    );

  }
}