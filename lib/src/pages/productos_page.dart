import 'package:flutter/material.dart';
import 'package:inventario_app/src/bloc/categoria_bloc.dart';
import 'package:inventario_app/src/bloc/producto_bloc.dart';
import 'package:inventario_app/src/models/categoria_model.dart';
import 'package:inventario_app/src/models/producto_model.dart';

// import 'package:inventario_app/src/bloc/categoria_bloc.dart';
// import 'package:inventario_app/src/bloc/producto_bloc.dart';
// import 'package:inventario_app/src/models/categoria_model.dart';
// import 'package:inventario_app/src/models/producto_model.dart';
// import 'package:inventario_app/src/providers/db_provider.dart';

import 'package:inventario_app/src/search/search_delegate.dart';
import 'package:inventario_app/src/widgets/card_producto_widget.dart';
import 'package:inventario_app/src/widgets/drawer_widget.dart';


class ProductosPage extends StatefulWidget {

  @override
  _ProductosPageState createState() => _ProductosPageState();
}

class _ProductosPageState extends State<ProductosPage> {

  //String _seleccion = 'Seleccione categoria';
  bool _activo;
  int _idOpcion = 0;
  int _idCategoriaSelected = 0;
  final categoriaBloc = new CategoriaBloc();
  final productoBloc = new ProductoBloc();

  @override
  void initState() {
    super.initState();
    _activo = false;
  }
  
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        title: Text('Inventario'),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
             icon: Icon(Icons.add ), 
             onPressed: () {
               Navigator.pushNamed(context, 'nuevo_producto');
             }
          ),
           IconButton(
             icon: Icon(Icons.search ), 
             onPressed: () {
               showSearch(context: context, delegate: DataSearch());
             }
          ),
        ],
      ),
      body: ListView(
        children: <Widget>[
          _crearMenuFilter(),
          Divider(),
          _crearCard(),
        ],
      ),
      drawer: _crearDrawer()
    );
  }

  Widget _crearDrawer() {
    return DrawerWidget();
  }

  Widget _crearCard(){
    if(_activo == false){
      return CardProductoWidget();
    }else{

      return _cardProductoCategoria(_idCategoriaSelected);
    }
  }

  Widget _cardProductoCategoria(int id) {

    productoBloc.obtenerProductosCatID(id);

    return StreamBuilder<List<ProductoModel>>(
      stream: productoBloc.productosStream ,
      builder: (BuildContext context, AsyncSnapshot<List<ProductoModel>> snapshot){
        
        if ( !snapshot.hasData ){
          return Center(child: CircularProgressIndicator());
        }
        final productos = snapshot.data;
        
        if ( productos.length == 0 ){
          return Center(child: Text('No hay información'));
        }

        return ListView.builder(
          shrinkWrap: true,
          physics: ScrollPhysics(),
          itemCount: productos.length,
          itemBuilder: (BuildContext context, int i) => Card(
            elevation: 5,
            child: ClipPath(
              child: Container(
                decoration: BoxDecoration(
                  border: Border(left: BorderSide(color: Color.fromRGBO(61, 59, 187, 1.0), width: 7.0))
                ),
                child: ListTile(
                  title: Text(productos[i].nombre),
                  subtitle: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(height: 5),
                      Text('Cantidad: ${productos[i].cantidad.toString()}'),
                      SizedBox(height: 5),
                      Text(r'Precio: $'+'${productos[i].precioVenta.toString()}')
                    ],
                  ),
                  trailing: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      _crearBotonesTrailing(context, productos[i] ,Colors.amber, Colors.amber[800], Icon(Icons.mode_edit, color: Colors.white,), 1),
                      SizedBox(width: 15.0),
                      _crearBotonesTrailing(context, productos[i] ,Colors.red, Colors.red[900], Icon(Icons.delete, color: Colors.white,), 0)
                    ],
                  ),
                ),
              ),
              clipper: ShapeBorderClipper(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(3)
                )
              ),
            ),
          )
        );
        
      },
    );
  }

  Widget _crearBotonesTrailing( BuildContext context, ProductoModel producto, Color backColor, Color splashColor, Icon icon, int flag){

    return ClipOval(
      child: Material(
        color: backColor, // button color
        child: InkWell(
          splashColor: splashColor, // inkwell color
          child: SizedBox(width: 40, height: 40, child: icon),
          onTap: () {
            if ( flag == 1 ){
              Navigator.pushNamed(context, 'editar_producto', arguments: {'producto':producto, 'id_categoria':producto.catId});
            }else if (flag == 0){
              productoBloc.borrarProducto(producto.idProducto);
            }
          },
        ),
      ),
    );

  }

  Widget _crearMenuFilter(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
          SizedBox(height: 10,),
          Container(
            margin: EdgeInsets.only(left: 5),
            child: Text(
              'Filtrar por Categoría',
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.w600
              ),
            ),
          ),
          Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Expanded(
                child: _dropdownCategoria(),
              ),
              Expanded(
                child: _crearSwitch(),
              )
            ],
          ),
        ],
    );
  }

  List<DropdownMenuItem<CategoriaModel>> getOpcionesDropdownCategoria( List<CategoriaModel> categorias ){

    List<DropdownMenuItem<CategoriaModel>> lista = new List();

    categorias.forEach((categoria) { 
      lista.add(DropdownMenuItem(
        value: categoria,
        child: Text('${categoria.nombre}', style: TextStyle(fontSize: 13.3),)
      ));
    });
    return lista; 

  }

  Widget _crearSwitch(){
    return SwitchListTile(
      value: _activo, 
      onChanged: ( value ) {

        setState(() {
          _activo = value;
        });

      }
    );
  }

  Widget _dropdownCategoria(){
    categoriaBloc.obtenerCategorias();
    
     return StreamBuilder<List<CategoriaModel>>(
       stream: categoriaBloc.categoriasStream,
       builder: (context, AsyncSnapshot<List<CategoriaModel>> snapshot){
    
         if ( !snapshot.hasData ){
           return Center(child: CircularProgressIndicator());
         }
         List<CategoriaModel> categorias = snapshot.data;
    
         if ( categorias.length == 0 ){
           return Center(child: Text('No hay información'));
         }

         _idCategoriaSelected = categorias[_idOpcion].idCategoria;

         return Row(
          children: <Widget>[
            Icon(Icons.category, color: _activo == false ? Colors.grey : Theme.of(context).primaryColor,),
            SizedBox(width: 10.0,),
            Expanded(
              child: DropdownButton(
                isExpanded: true,
                iconDisabledColor: Colors.grey,
                value: categorias[_idOpcion],
                items: getOpcionesDropdownCategoria(categorias),
                onChanged: _activo == false ? null : (opt) {
                  for (var i = 0; i < categorias.length; i++) {
                    if(opt.idCategoria == categorias[i].idCategoria){
                      _idOpcion = i; 
                    }
                  }
                  setState(() { });
                  _idCategoriaSelected = categorias[_idOpcion].idCategoria;
                },
              ),
            )
          ],
        );
       },
     );
   }
   
}