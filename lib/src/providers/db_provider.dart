import 'dart:io';

import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';

import 'package:inventario_app/src/models/categoria_model.dart';
import 'package:inventario_app/src/models/producto_model.dart';
import 'package:inventario_app/src/models/venta_model.dart';
import 'package:inventario_app/src/models/gasto_model.dart';
import 'package:inventario_app/src/models/otro_gasto_model.dart';
export 'package:inventario_app/src/models/categoria_model.dart';
export 'package:inventario_app/src/models/producto_model.dart';
export 'package:inventario_app/src/models/venta_model.dart';
export 'package:inventario_app/src/models/gasto_model.dart';
export 'package:inventario_app/src/models/otro_gasto_model.dart';

class DBProvider{

  static Database _database;
  static final DBProvider db = DBProvider._();

  DBProvider._();

  Future<Database> get database async {

    if ( _database != null ) return _database;

    _database = await initDB();
    return _database;

  }

  static Future _onConfigure(Database db) async {
    await db.execute('PRAGMA foreign_keys = ON');
  }

  initDB() async {

    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    final path = join( documentsDirectory.path, 'inventory.db' );
    
    return await openDatabase(
      path,
      version: 1,
      onOpen: ( db ){},
      onCreate: ( Database db, int version ) async {
        Batch dbBatch = db.batch();
        dbBatch.execute(
          'CREATE TABLE IF NOT EXISTS Categoria('
          ' id_categoria INTEGER PRIMARY KEY,'
          ' nombre_categoria TEXT NOT NULL'
          ')'
        );
        dbBatch.execute(
          'CREATE TABLE IF NOT EXISTS Producto('
              'id_producto INTEGER PRIMARY KEY,'
              'nombre_producto TEXT NOT NULL,'
              'cat_id INTEGER,'
              'precio_venta REAL NOT NULL,'
              'precio_compra REAL NOT NULL,'
              'cantidad INTEGER NOT NULL,'
              'FOREIGN KEY(cat_id) references Categoria(id_categoria)'
          ')'
        );
        dbBatch.execute(
          'CREATE TABLE IF NOT EXISTS Venta('
            'id_venta INTEGER PRIMARY KEY,'
            'id_producto INTEGER,'
            'fecha TEXT NOT NULL,'
            'cantidad INTEGER NOT NULL,'
            'total_venta REAL NOT NULL,'
            'FOREIGN KEY(id_producto) references Producto(id_producto)'
          ')'
        );
        dbBatch.execute(
          'CREATE TABLE IF NOT EXISTS GastosProductos('
            'id_gasto INTEGER PRIMARY KEY,'
            'id_producto INTEGER,'
            'fecha TEXT NOT NULL,'
            'cantidad INTEGER NOT NULL,'
            'total_gasto REAL NOT NULL,'
            'FOREIGN KEY(id_producto) references Producto(id_producto)'
          ')'
        );
        dbBatch.execute(
          'CREATE TABLE IF NOT EXISTS GastosOtros('
            'id_gasto_otro INTEGER PRIMARY KEY,'
            'concepto VARCHAR(30) NOT NULL,'
            'total_gasto DECIMAL(7,2) NOT NULL, '
            'fecha VARCHAR(15) NOT NULL'
          ')'
        );

        await dbBatch.commit();
      },
      onConfigure: _onConfigure
    );

  }

  // INSERTS - Crear Registros
  nuevaCategoria( CategoriaModel nuevaCategoria ) async{

    final db  = await database;

    final res = db.insert('Categoria', nuevaCategoria.toJson() );

    return res;

  }

  nuevoProducto( ProductoModel nuevoProducto ) async {

    final db  = await database;
    final res = db.insert('Producto', nuevoProducto.toJson() );
    return res;

  }

  nuevaVenta(VentaModel nuevaVenta) async {
    
    final db  = await database;
    final res = db.insert('Venta', nuevaVenta.toJson());
    return res;

  }

  nuevaGastoProducto( GastoModel nuevaGasto) async {
    
    final db  = await database;
    final res = db.insert('GastosProductos', nuevaGasto.toJson());
    return res;

  }

  nuevoGastoOtro( OtroGastoModel nuevoGasto ) async {

    final db  = await database;
    final res = db.insert('GastosOtros', nuevoGasto.toJson());
    return res;


  }


  // SELECTS - Obtener información
  Future<List<CategoriaModel>> getTodasCategorias() async {

    final db  = await database;

    final res = await db.query('Categoria');

    List<CategoriaModel> list = res.isNotEmpty 
                              ? res.map( (c) => CategoriaModel.fromJson(c)).toList()
                              : [];

    return list;

  }

  Future<List<ProductoModel>>getTodosProductos() async {

    final db  = await database;

    final res = await db.query('Producto');

    List<ProductoModel> list = res.isNotEmpty 
                              ? res.map( (c) => ProductoModel.fromJson(c)).toList()
                              : [];

    return list;

  }

  Future<List<ProductoModel>>getProductosCatID(int catId) async {

    final db  = await database;

    final res = await db.query('Producto', where: 'cat_id = ?', whereArgs: [catId]);

    List<ProductoModel> list = res.isNotEmpty 
                              ? res.map( (c) => ProductoModel.fromJson(c)).toList()
                              : [];

    return list;

  }

  Future<List<ProductoModel>>getProductosNombre(String nombre) async {

    final db  = await database;

    final res = await db.query('Producto', where: "nombre_producto LIKE ?", whereArgs: ['%$nombre%']);

    List<ProductoModel> list = res.isNotEmpty 
                              ? res.map( (c) => ProductoModel.fromJson(c)).toList()
                              : [];

    return list;

  }

  Future<List<VentaModel>> getTodasVentas() async {

    final db  = await database;

    final res = await db.query('Venta');

    List<VentaModel> list = res.isNotEmpty 
                              ? res.map( (c) => VentaModel.fromJson(c)).toList()
                              : [];

    return list;

  }

  Future<List<double>> getTotalVentas() async {

    final db  = await database;

    final res = await db.rawQuery('SELECT SUM(total_venta) FROM Venta');

    List<double> list = res.isNotEmpty 
                              ? res.map( (c) => VentaModel.fromJson(c)).toList()
                              : [];

    return list;

  }

  Future<List<GastoModel>> getTodosGastosP() async {

    final db  = await database;

    final res = await db.query('GastosProductos');

    List<GastoModel> list = res.isNotEmpty 
                              ? res.map( (c) => GastoModel.fromJson(c)).toList()
                              : [];

    return list;

  }

  Future<List<OtroGastoModel>> getTodosGastosOtros() async {

    final db  = await database;

    final res = await db.query('GastosOtros');

    List<OtroGastoModel> list = res.isNotEmpty 
                              ? res.map( (c) => OtroGastoModel.fromJson(c)).toList()
                              : [];

    return list;

  }

  Future<List<double>> getTotalGastoP() async {

    final db  = await database;

    final res = await db.rawQuery('SELECT SUM(total_gasto) FROM GastosProductos');

    List<double> list = res.isNotEmpty 
                              ? res.map( (c) => GastoModel.fromJson(c)).toList()
                              : [];

    return list;

  }

   Future<List<double>> getTotalGastoOtros() async {

    final db  = await database;

    final res = await db.rawQuery('SELECT SUM(total_gasto) FROM GastosOtros');

    List<double> list = res.isNotEmpty 
                              ? res.map( (c) => OtroGastoModel.fromJson(c)).toList()
                              : [];

    return list;

  }

  // UPDATES - Actualizar registros
  Future<int> updateCategoria( CategoriaModel nuevaCategoria ) async{

    final db  = await database;

    final res = await db.update('Categoria', nuevaCategoria.toJson(), where: 'id_categoria = ?', whereArgs: [nuevaCategoria.idCategoria]);

    return res;

  }

  Future<int> updateProducto( ProductoModel nuevoProducto ) async{

    final db  = await database;

    final res = await db.update('Producto', nuevoProducto.toJson(), where: 'id_producto = ?', whereArgs: [nuevoProducto.idProducto]);

    return res;

  }

   Future<int> updateVenta( VentaModel nuevaVenta ) async{

    final db  = await database;

    final res = await db.update('Venta', nuevaVenta.toJson(), where: 'id_venta = ?', whereArgs: [nuevaVenta.idVenta]);

    return res;

  }


  Future<int> updateGastoProducto( GastoModel nuevoGasto ) async{

    final db  = await database;

    final res = await db.update('GastosProductos', nuevoGasto.toJson(), where: 'id_gasto = ?', whereArgs: [nuevoGasto.idGasto]);

    return res;

  }

  Future<int> updateOtroGasto( OtroGastoModel nuevoGasto ) async{

    final db  = await database;

    final res = await db.update('GastosOtros', nuevoGasto.toJson(), where: 'id_gasto_otro = ?', whereArgs: [nuevoGasto.idGastoOtro]);

    return res;

  }

  Future<int> updateStockProductoVentas( int vendido, int idProducto) async{

    final db  = await database;

    final res = await db.rawUpdate('UPDATE Producto SET cantidad = cantidad - $vendido  WHERE id_producto=$idProducto');

    return res;

  }

  Future<int> updateCancelarVenta( int vendido, int idProducto) async{

    final db  = await database;

    final res = await db.rawUpdate('UPDATE Producto SET cantidad = cantidad + $vendido  WHERE id_producto=$idProducto');

    return res;

  }


  // DELETES - Eliminar registros
  Future<int> deleteCategoria( int id ) async{

    final db  = await database;

    final res = await db.delete('Categoria', where: 'id_categoria = ?', whereArgs: [id]);

    return res;

  }

  Future<int> deleteProducto( int id ) async{

    final db  = await database;

    final res = await db.delete('Producto', where: 'id_producto = ?', whereArgs: [id]);

    return res;

  }

  Future<int> deleteVenta( int id ) async{

    final db  = await database;

    final res = await db.delete('Venta', where: 'id_venta = ?', whereArgs: [id]);

    return res;

  }

  Future<int> deleteGastoProducto( int id ) async{

    final db  = await database;

    final res = await db.delete('GastosProductos', where: 'id_gasto = ?', whereArgs: [id]);

    return res;

  }

  Future<int> deleteOtroGasto( int id ) async{

    final db  = await database;

    final res = await db.delete('GastosOtros', where: 'id_gasto_otro = ?', whereArgs: [id]);

    return res;

  }

}