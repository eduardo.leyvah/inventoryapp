import 'dart:async';

import 'package:inventario_app/src/providers/db_provider.dart'; 

class GastoProductoBloc{

  static final GastoProductoBloc _singleton = new GastoProductoBloc._internal();

  factory GastoProductoBloc(){
    return _singleton;
  }

   GastoProductoBloc._internal() {
    //  Obtener los Scans de la Base de Datos
    obtenerGastoProducto();

   }

   // Controlador del Stream 
   final _controllerGastoProdcuto = StreamController<List<GastoModel>>.broadcast();

   Stream<List<GastoModel>> get gastoProdcutoStream => _controllerGastoProdcuto.stream;

   


  dispose() {
    _controllerGastoProdcuto?.close();
  }

  // Obtener Gastos Productos

  obtenerGastoProducto() async {

    _controllerGastoProdcuto.sink.add( await DBProvider.db.getTodosGastosP() );

  }
  

  // Agregar Gastos Productos
  agregarGastoP( GastoModel gasto ) async {

    await DBProvider.db.nuevaGastoProducto(gasto);
    obtenerGastoProducto();

  }

  // Editar Gastos Productos
   updateGastoP( GastoModel gasto ) async {

     await DBProvider.db.updateGastoProducto(gasto);
     obtenerGastoProducto();

   }

   updateCancelarGasto( int cantidad, int idProducto ) async {

     await DBProvider.db.updateStockProductoVentas(cantidad, idProducto);

   }

   updateStockProducto( int cantidad, int idProducto ) async {

     await DBProvider.db.updateCancelarVenta(cantidad, idProducto);

   }

  // Borrar Gastos Productos
   borrarGastoP( int id ) async {

     await DBProvider.db.deleteGastoProducto(id);
     obtenerGastoProducto();

   }


}