import 'dart:async';

import 'package:inventario_app/src/providers/db_provider.dart'; 

class OtrosGastosBloc{

  static final OtrosGastosBloc _singleton = new OtrosGastosBloc._internal();

  factory OtrosGastosBloc(){
    return _singleton;
  }

   OtrosGastosBloc._internal() {
    //  Obtener los Scans de la Base de Datos
    obtenerOtroGasto();

   }

   // Controlador del Stream 
   final _otroGastoController = StreamController<List<OtroGastoModel>>.broadcast();

   Stream<List<OtroGastoModel>> get otroGastoStream => _otroGastoController.stream;

   


  dispose() {
    _otroGastoController?.close();
  }

  // Obtener Gastos Otros

  obtenerOtroGasto() async {

    _otroGastoController.sink.add( await DBProvider.db.getTodosGastosOtros() );

  }
  

  // Agregar Gastos Otros
  agregarOtroGasto( OtroGastoModel gasto ) async {

    await DBProvider.db.nuevoGastoOtro(gasto);
    obtenerOtroGasto();

  }

  // Editar Gastos Otros
   updateOtroGasto( OtroGastoModel gasto ) async {

     await DBProvider.db.updateOtroGasto(gasto);
     obtenerOtroGasto();

   }

   updateCancelarGasto( int cantidad, int idProducto ) async {

     await DBProvider.db.updateStockProductoVentas(cantidad, idProducto);

   }

   updateStockProducto( int cantidad, int idProducto ) async {

     await DBProvider.db.updateCancelarVenta(cantidad, idProducto);

   }

  // Borrar Gastos Otros
   borrarOtroGasto( int id ) async {

     await DBProvider.db.deleteOtroGasto(id);
     obtenerOtroGasto();

   }


}