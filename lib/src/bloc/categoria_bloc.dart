import 'dart:async';

import 'package:inventario_app/src/providers/db_provider.dart';

class CategoriaBloc{

  static final CategoriaBloc _singleton = new CategoriaBloc._internal();

  factory CategoriaBloc(){
    return _singleton;
  }

   CategoriaBloc._internal() {
    //  Obtener los Scans de la Base de Datos
    obtenerCategorias();

   }

   // Controlador del Stream 
   final _categoriasController = StreamController<List<CategoriaModel>>.broadcast();

   Stream<List<CategoriaModel>> get categoriasStream => _categoriasController.stream;

  dispose() {
    _categoriasController?.close();
  }

  // Obtener Categorias
  obtenerCategorias() async {

    _categoriasController.sink.add( await DBProvider.db.getTodasCategorias() );

  }

  // Agregar Categoria
  agregarCategoria( CategoriaModel categoria ) async {

    await DBProvider.db.nuevaCategoria(categoria);
    obtenerCategorias();

  }

  // Editar Categoria
  updateCategoria( CategoriaModel categoria ) async {

    await DBProvider.db.updateCategoria(categoria);
    obtenerCategorias();

  }

  // Borrar Categoria
  borrarCategoria( int id ) async {

    await DBProvider.db.deleteCategoria(id);
    obtenerCategorias();

  }


}