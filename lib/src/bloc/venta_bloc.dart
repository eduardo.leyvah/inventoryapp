import 'dart:async';

import 'package:inventario_app/src/providers/db_provider.dart'; 

class VentasBloc{

  static final VentasBloc _singleton = new VentasBloc._internal();

  factory VentasBloc(){
    return _singleton;
  }

   VentasBloc._internal() {
    //  Obtener los Scans de la Base de Datos
    obtenerVenta();

   }

   // Controlador del Stream 
   final _ventaController = StreamController<List<VentaModel>>.broadcast();

   Stream<List<VentaModel>> get ventaStream => _ventaController.stream;

   


  dispose() {
    _ventaController?.close();
  }

  // Obtener Ventas

  obtenerVenta() async {

    _ventaController.sink.add( await DBProvider.db.getTodasVentas() );

  }
  

  // Agregar Venta
  agregarVenta( VentaModel venta ) async {

    await DBProvider.db.nuevaVenta(venta);
    obtenerVenta();


  }

  // Editar Venta
   updateVenta( VentaModel venta ) async {

     await DBProvider.db.updateVenta(venta);
     obtenerVenta();

   }

   updateStock( int cantidad, int idProducto ) async {

     await DBProvider.db.updateStockProductoVentas(cantidad, idProducto);

   }

   updateCancelarVenta( int cantidad, int idProducto ) async {

     await DBProvider.db.updateCancelarVenta(cantidad, idProducto);

   }

  // Borrar Venta
   borrarVenta( int id ) async {

     await DBProvider.db.deleteVenta(id);
     obtenerVenta();

   }


}