import 'dart:async';

import 'package:inventario_app/src/providers/db_provider.dart'; 

class ProductoBloc{

  static final ProductoBloc _singleton = new ProductoBloc._internal();

  factory ProductoBloc(){
    return _singleton;
  }

   ProductoBloc._internal() {
    //  Obtener los Scans de la Base de Datos
    obtenerProductos();

   }

   // Controlador del Stream 
   final _productosController = StreamController<List<ProductoModel>>.broadcast();

   Stream<List<ProductoModel>> get productosStream => _productosController.stream;


  dispose() {
    _productosController?.close();
  }

  // Obtener Productos
  obtenerProductos() async {

    _productosController.sink.add( await DBProvider.db.getTodosProductos() );

  }

  obtenerProductosCatID(int id) async {

    _productosController.sink.add( await DBProvider.db.getProductosCatID(id) );

  }

  // Agregar Producto
  agregarProducto( ProductoModel producto ) async {

    await DBProvider.db.nuevoProducto(producto);
    obtenerProductos();

  }

  // Editar Producto
  updateProducto( ProductoModel producto ) async {

    await DBProvider.db.updateProducto(producto);
    obtenerProductos();

  }

  // Borrar Producto
  borrarProducto( int id ) async {

    await DBProvider.db.deleteProducto(id);
    obtenerProductos();

  }


}