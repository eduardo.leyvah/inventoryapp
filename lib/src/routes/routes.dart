import 'package:flutter/material.dart';
import 'package:inventario_app/src/pages/categorias_page.dart';
import 'package:inventario_app/src/pages/detalle_producto_page.dart';
import 'package:inventario_app/src/pages/editar_gasto_page.dart';
import 'package:inventario_app/src/pages/editar_ogasto_page.dart';
import 'package:inventario_app/src/pages/editar_producto_page.dart';
import 'package:inventario_app/src/pages/editar_venta_page.dart';

import 'package:inventario_app/src/pages/login_page.dart';
import 'package:inventario_app/src/pages/home_page.dart';
import 'package:inventario_app/src/pages/dashboard_page.dart';
import 'package:inventario_app/src/pages/nueva_venta_page.dart';
import 'package:inventario_app/src/pages/nuevo_gasto_otros.dart';
import 'package:inventario_app/src/pages/nuevo_gasto_page.dart';
import 'package:inventario_app/src/pages/nuevo_producto_page.dart';
import 'package:inventario_app/src/pages/productos_page.dart';
import 'package:inventario_app/src/pages/ventas_page.dart';
import 'package:inventario_app/src/pages/gastos_page.dart';


Map<String,WidgetBuilder> getApplicationRoutes(){
  
  return <String,WidgetBuilder>{
    'login'            : (BuildContext context) => LoginPage(),
    'home'             : (BuildContext context) => HomePage(),
    'categorias'       : (BuildContext context) => CategoriasPage(),
    'productos'        : (BuildContext context) => ProductosPage(),
    'dashboard'        : (BuildContext context) => DashboardPage(),
    'ventas'           : (BuildContext context) => VentasPage(),
    'gastos'           : (BuildContext context) => GastosPage(),
    'nuevo_producto'   : (BuildContext context) => NuevoProductoPage(),
    'editar_producto'  : (BuildContext context) => EditarProductoPage(),
    'detalle'          : (BuildContext context) => DetalleProductoPage(),
    'nueva_venta'      : (BuildContext context) => NuevaVentaPage(),
    'editar_venta'     : (BuildContext context) => EditarVentaPage(),
    'nuevo_gasto'      : (BuildContext context) => NuevoGastoPage(),
    'nuevo_gasto_otro' : (BuildContext context) => GastoOtroPage(),
    'editar_gastop'    : (BuildContext context) => EditarGastoPage(),
    'editar_ogasto'    : (BuildContext context) => EditarOtroGastoPage()
  };

}
